package com.onregs.recyclerviewexample.json;

import android.support.annotation.Nullable;
import android.util.JsonReader;

import com.onregs.recyclerviewexample.data.Comment;
import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.MediaContent;
import com.onregs.recyclerviewexample.data.User;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by vadim on 16.11.2015.
 */
public class CustomJsonReader {

    public static final String DATA = "results";
    public static final String ID = "objectId";
    public static final String UTF_8 = "UTF-8";

    private ArrayList<Feed> mFeeds;
    private ArrayList<MediaContent> mMediaContent;
    private ArrayList<Comment> mComments;
    private User mUser;

    private int mType;

    public static final int READ_FEEDS = 1;
    public static final int READ_MEDIA_CONTENT = 2;
    public static final int READ_COMMENTS = 3;
    public static final int READ_USER = 4;

    public CustomJsonReader(@Nullable ArrayList data, int type) {
        switch (type) {
            case READ_FEEDS:
                this.mFeeds = data;
                break;
            case READ_MEDIA_CONTENT:
                this.mMediaContent = data;
                break;
            case READ_COMMENTS:
                this.mComments = data;
                break;
        }
        this.mType = type;
    }

    public CustomJsonReader(User user, int type) {
        this.mUser = user;
        this.mType = type;
    }


    public void fillData(InputStream inputStream) throws IOException {
        JsonReader reader = null;
        try {
            reader = new JsonReader(new InputStreamReader(inputStream, UTF_8));
            reader.beginObject();
            String objName = reader.nextName();

            if (objName.equals(DATA)) {
                readDataArray(reader);
                reader.endObject();
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    private void readDataArray(JsonReader reader) throws IOException {
        reader.beginArray();
        switch (mType) {
            case READ_FEEDS:
                while (reader.hasNext()) {
                    mFeeds.add(readFeedObject(reader));
                }
                break;
            case READ_MEDIA_CONTENT:
                while (reader.hasNext()) {
                    mMediaContent.add(readMediaContentObject(reader));
                }
                break;
            case READ_COMMENTS:
                while (reader.hasNext()) {
                    mComments.add(readCommentObject(reader));
                }
                break;
            case READ_USER:
                if (reader.hasNext()) {
                    readUser(reader);
                }
                break;
        }
        reader.endArray();
    }

    private Feed readFeedObject(JsonReader reader) throws IOException {
        Feed feed = new Feed();

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case ID:
                    feed.setId(reader.nextString());
                    break;
                case Feed.FeedEntry.COLUMN_FEED_TITTLE:
                    feed.setTitle(reader.nextString());
                    break;
                case Feed.FeedEntry.COLUMN_FEED_DESCRIPTION:
                    feed.setDescription(reader.nextString());
                    break;
                case Feed.FeedEntry.COLUMN_IMAGE_URL:
                    feed.setImageUrl(reader.nextString());
                    break;
                case Feed.FeedEntry.COLUMN_FEED_RATING:
                    feed.setRating(reader.nextInt());
                    break;
                case Feed.FeedEntry.COLUMN_FEED_CREATED_AT:
                    feed.setCreatedAt(reader.nextLong());
                    break;
                case Feed.FeedEntry.COLUMN_FEED_LINK:
                    feed.setLink(reader.nextString());
                    break;
                case Feed.FeedEntry.COLUMN_FEED_TYPE:
                    feed.setFeedType(reader.nextString());
                    break;
                case Feed.FeedEntry.COLUMN_FEED_COMMENTS_COUNT:
                    feed.setCommentsCount(reader.nextInt());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return feed;
    }

    private MediaContent readMediaContentObject(JsonReader reader) throws IOException {
        MediaContent mediaContent = new MediaContent();

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case ID:
                    mediaContent.setId(reader.nextString());
                    break;
                case MediaContent.MediaContentEntry.COLUMN_FIELD_LINK:
                    mediaContent.setLink(reader.nextString());
                    break;
                case MediaContent.MediaContentEntry.COLUMN_MEDIA_TYPE:
                    mediaContent.setType(reader.nextString());
                    break;
                case MediaContent.MediaContentEntry.COLUMN_FEED_ID:
                    mediaContent.setFeedId(reader.nextInt());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return mediaContent;
    }

    private Comment readCommentObject(JsonReader reader) throws IOException {
        Comment comment = new Comment();

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case ID:
                    comment.setId(reader.nextString());
                    break;
                case Comment.CommentsContentEntry.COLUMN_NAME:
                    comment.setName(reader.nextString());
                    break;
                case Comment.CommentsContentEntry.COLUMN_LAST_NAME:
                    comment.setLastName(reader.nextString());
                    break;
                case Comment.CommentsContentEntry.COLUMN_COMMENT:
                    comment.setComment(reader.nextString());
                    break;
                case Comment.CommentsContentEntry.COLUMN_COMMENT_CREATED_AT:
                    comment.setCreatedAt(reader.nextLong());
                    break;
                case MediaContent.MediaContentEntry.COLUMN_FEED_ID:
                    comment.setFeedId(reader.nextString());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return comment;
    }

    private void readUser(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case ID:
                    mUser.setId(reader.nextString());
                    break;
                case User.UserEntry.COLUMN_USER_NAME:
                    mUser.setUserName(reader.nextString());
                    break;
                case User.UserEntry.COLUMN_EMAIL:
                    mUser.setEmail(reader.nextString());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        mUser.setAuthorized(true);
        reader.endObject();
    }
}
