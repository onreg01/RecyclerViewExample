package com.onregs.recyclerviewexample.data.loader;

import android.content.Context;

import com.onregs.recyclerviewexample.R;

/**
 * Created by vadim on 12.12.2015.
 */
public class LoaderAnswer {
    protected boolean mResult;
    protected String mMessage;
    protected Context mContext;

    public static final int SUCCESSFULLY = 0;
    public static final int EMPTY_DATA = 1;
    public static final int CONNECTION_ERROR = 2;
    public static final int PARSE_DATA_ERROR = 3;

    public LoaderAnswer(boolean result, int typeAnswer, Context context) {
        this.mResult = result;
        this.mContext = context;
        this.setMessage(typeAnswer);
    }

    public void setMessage(int typeAnswer) {
        switch (typeAnswer) {
            case EMPTY_DATA:
                mMessage = mContext.getString(R.string.empty_data);
                break;
            case CONNECTION_ERROR:
                mMessage = mContext.getString(R.string.connection_error);
                break;
            case PARSE_DATA_ERROR:
                mMessage = mContext.getString(R.string.parse_data_error);
                break;
            case SUCCESSFULLY:
                mMessage = mContext.getString(R.string.successfully);
                break;
        }
    }

    public boolean getResult() {
        return mResult;
    }

    public String getMessage() {
        return mMessage;
    }
}