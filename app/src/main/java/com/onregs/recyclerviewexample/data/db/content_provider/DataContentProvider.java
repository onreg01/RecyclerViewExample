package com.onregs.recyclerviewexample.data.db.content_provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.onregs.recyclerviewexample.data.ChangedFeed;
import com.onregs.recyclerviewexample.data.Comment;
import com.onregs.recyclerviewexample.data.FavoriteFeed;
import com.onregs.recyclerviewexample.data.MediaContent;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;


public class DataContentProvider extends SimpleContentProvider {

    public static final String AUTHORITY = "com.onregs.providers";
    public static final Uri CONTENT_URI_COMMENTS = Uri.parse("content://" + AUTHORITY + "/" + Comment.CommentsContentEntry.TABLE_COMMENTS);
    public static final Uri CONTENT_URI_MEDIA =
            Uri.parse("content://" + AUTHORITY + "/" + MediaContent.MediaContentEntry.TABLE_MEDIA_CONTENT);

    public static final Uri CONTENT_URI_FAVORITES =
            Uri.parse("content://" + AUTHORITY + "/" + FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS);

    public static final Uri CONTENT_URI_CHANGED_FEEDS =
            Uri.parse("content://" + AUTHORITY + "/" + ChangedFeed.ChangedFeedEntry.TABLE_CHANGED_FEEDS);

    static final int URI_MEDIA = 1;
    static final int URI_COMMENTS = 2;
    static final int URI_FAVORITE_FEEDS = 3;
    static final int URI_CHANGED_FEEDS = 4;

    private DataSqliteOpenHelper mDataSqliteOpenHelper;

    private static final UriMatcher sUriMatcher;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, MediaContent.MediaContentEntry.TABLE_MEDIA_CONTENT, URI_MEDIA);
        sUriMatcher.addURI(AUTHORITY, Comment.CommentsContentEntry.TABLE_COMMENTS, URI_COMMENTS);
        sUriMatcher.addURI(AUTHORITY, FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS, URI_FAVORITE_FEEDS);
        sUriMatcher.addURI(AUTHORITY, ChangedFeed.ChangedFeedEntry.TABLE_CHANGED_FEEDS, URI_CHANGED_FEEDS);
    }


    @Override
    public boolean onCreate() {
        mDataSqliteOpenHelper = DataSqliteOpenHelper.getInstance(getContext());
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        String tableName = null;
        int machUri = sUriMatcher.match(uri);

        switch (machUri) {
            case URI_MEDIA:
                tableName = MediaContent.MediaContentEntry.TABLE_MEDIA_CONTENT;
                break;
            case URI_COMMENTS:
                tableName = Comment.CommentsContentEntry.TABLE_COMMENTS;
                break;
            case URI_CHANGED_FEEDS:
                tableName = ChangedFeed.ChangedFeedEntry.TABLE_CHANGED_FEEDS;
                break;
        }

        Cursor cursor = mDataSqliteOpenHelper.getWritableDatabase().query(
                tableName, projection, selection, selectionArgs, null, null, sortOrder);

        Context context = getContext();

        if (context != null) {
            if (machUri == URI_COMMENTS) {
                cursor.setNotificationUri(context.getContentResolver(), CONTENT_URI_COMMENTS);
            } else if (machUri == URI_MEDIA) {
                cursor.setNotificationUri(context.getContentResolver(), CONTENT_URI_MEDIA);
            }
        }
        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        int machUri = sUriMatcher.match(uri);
        String table = null;
        switch (machUri) {
            case URI_FAVORITE_FEEDS:
                table = FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS;
                break;
            case URI_CHANGED_FEEDS:
                table = ChangedFeed.ChangedFeedEntry.TABLE_CHANGED_FEEDS;
                break;
        }
        mDataSqliteOpenHelper.getWritableDatabase().execSQL(
                DataSqliteOpenHelper.CustomQueryBuilder.getInsertQueryWithSelection(table, null, values));

        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        String table = null;
        int machUri = sUriMatcher.match(uri);
        switch (machUri) {
            case URI_FAVORITE_FEEDS:
                table = FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS;
                break;
            case URI_CHANGED_FEEDS:
                table = ChangedFeed.ChangedFeedEntry.TABLE_CHANGED_FEEDS;
                break;

        }
        return mDataSqliteOpenHelper.getWritableDatabase().delete(table, selection, selectionArgs);
    }
}
