package com.onregs.recyclerviewexample.data;

import android.provider.BaseColumns;

/**
 * Created by vadim on 13.12.2015.
 */
public class Comment {
    private String mId;
    private String mName;
    private String mLastName;
    private String mComment;
    private long mCreatedAt;
    private String mFeedId;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        this.mLastName = lastName;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        this.mComment = comment;
    }

    public long getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(long createdAt) {
        this.mCreatedAt = createdAt;
    }

    public String getFeedId() {
        return mFeedId;
    }

    public void setFeedId(String feedId) {
        this.mFeedId = feedId;
    }

    public static abstract class CommentsContentEntry implements BaseColumns {

        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_LAST_NAME = "last_name";
        public static final String COLUMN_COMMENT = "comment";
        public static final String COLUMN_COMMENT_CREATED_AT = "created_at";
        public static final String COLUMN_FEED_ID = "feed_id";


        public static final String TABLE_COMMENTS = "comments";

        public static final String CREATE_COMMENTS_TABLE =
                "create table " + TABLE_COMMENTS + "(" +
                        _ID + " text primary key, " +
                        COLUMN_NAME + " text, " +
                        COLUMN_LAST_NAME + " text, " +
                        COLUMN_COMMENT + " text, " +
                        COLUMN_COMMENT_CREATED_AT + " long, " +
                        COLUMN_FEED_ID + " integer " +
                        ");";
    }
}
