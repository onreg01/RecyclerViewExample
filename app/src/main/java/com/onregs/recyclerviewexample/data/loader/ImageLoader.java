package com.onregs.recyclerviewexample.data.loader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.LruCache;

import com.onregs.recyclerviewexample.Const;
import com.onregs.recyclerviewexample.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

/**
 * Created by vadim on 20.11.2015.
 */
public class ImageLoader {

    private Context mContext;

    private ImageLoader(Context context) {
        this.mContext = context;
    }

    private static volatile ImageLoader sImageLoader;

    public static ImageLoader getInstance(Context context) {
        if (sImageLoader == null) {
            synchronized (ImageLoader.class) {
                if (sImageLoader == null) {
                    sImageLoader = new ImageLoader(context);
                }
            }
        }
        return sImageLoader;
    }

    public static final int CACHE_SIZE = 5 * 1024 * 1024;// 5mb
    public static final int COMPRESS_QUALITY = 100;
    public static final String IMAGE_FORMAT = ".png";
    public static final String ORIGINAL_IMAGE = "_original";
    public static final String RESIZED_IMAGE = "_resized";


    private LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(CACHE_SIZE) {
        protected int sizeOf(String key, Bitmap value) {
            return value.getByteCount();
        }
    };

    @Nullable
    public AsyncTask load(String url, boolean cacheCard, Callbacks callbacks) {
        if (url != null) {
            String imageUrl = getClearUrlPass(url + ORIGINAL_IMAGE);
            Bitmap bitmap = mCache.get(imageUrl);
            if (bitmap != null) {
                callbacks.successfully(bitmap);
            } else {
                return new LoadImageFromUrl(url, cacheCard, callbacks).execute();
            }
        }
        return null;
    }

    @Nullable
    public AsyncTask load(String url, int targetWidth, int targetHeight, boolean cacheCard, Callbacks callbacks) {
        if (url != null) {
            String imageUrl = getClearUrlPass(url + RESIZED_IMAGE);
            Bitmap bitmap = mCache.get(imageUrl);
            if (bitmap != null) {
                callbacks.successfully(bitmap);
            } else {
                return new LoadImageFromUrl(url, targetWidth, targetHeight, cacheCard, callbacks).execute();
            }
        }
        return null;
    }

    private String getClearUrlPass(String pass) {
        return pass.replace("/", "").replace(":", "") + IMAGE_FORMAT;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public void put(String url, Bitmap image) {
        mCache.put(url, image);
    }

    public interface Callbacks {
        void successfully(Bitmap bitmap);

        void failLoad(Bitmap bitmap);
    }

    private final class LoadImageFromUrl extends AsyncTask<Void, Void, Bitmap> {

        private String mUrl;
        private WeakReference<Callbacks> mCallbacks;
        private boolean mWithResize;
        private int mTargetWidth;
        private int mTargetHeight;
        private boolean mCacheCard;

        public LoadImageFromUrl(String url, boolean cacheCard, Callbacks callbacks) {
            this.mUrl = url;
            this.mCallbacks = new WeakReference<>(callbacks);
            this.mCacheCard = cacheCard;
        }

        public LoadImageFromUrl(String url, int targetWidth, int targetHeight, boolean cacheCard, Callbacks callbacks) {
            this.mUrl = url;
            this.mCallbacks = new WeakReference<>(callbacks);
            this.mTargetWidth = targetWidth;
            this.mTargetHeight = targetHeight;
            this.mWithResize = true;
            this.mCacheCard = cacheCard;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap bitmap;
            if (mWithResize) {
                bitmap = getImageExternalCache(getClearUrlPass(mUrl + RESIZED_IMAGE));
                if (bitmap == null) {
                    bitmap = loadWithResize();
                }
            } else {
                bitmap = getImageExternalCache(getClearUrlPass(mUrl + ORIGINAL_IMAGE));
                if (bitmap == null) {
                    bitmap = loadWithoutResize();
                }
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Callbacks callbacks = mCallbacks.get();
            if (callbacks != null) {
                if (bitmap != null) {
                    callbacks.successfully(bitmap);
                } else {
                    callbacks.failLoad(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_error_outline_black_48dp));
                }
            }
        }

        public Bitmap loadWithoutResize() {
            Bitmap bitmap = null;
            InputStream inputStream = null;
            try {
                inputStream = new URL(mUrl).openConnection().getInputStream();
                bitmap = BitmapFactory.decodeStream(inputStream, null, null);
                String imageUrl = getClearUrlPass(mUrl + ORIGINAL_IMAGE);
                put(getClearUrlPass(imageUrl), bitmap);
                if (mCacheCard) {
                    writeImageToCard(bitmap, imageUrl);
                }
            } catch (IOException e) {
                Log.d(Const.ERROR, e.getMessage(), e);
            } finally {
                if (inputStream != null) {
                    closeStream(inputStream);
                }
            }
            return bitmap;
        }

        public Bitmap loadWithResize() {
            Bitmap resizedBitmap = null;
            InputStream inputStream = null;
            BitmapFactory.Options options = new BitmapFactory.Options();
            try {
                inputStream = new URL(mUrl).openConnection().getInputStream();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, null, options);
                inputStream.close();

                setOptions(options);

                inputStream = new URL(mUrl).openConnection().getInputStream();

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
                resizedBitmap = Bitmap.createScaledBitmap(bitmap, mTargetWidth, mTargetHeight, true);

                String imageUrl = getClearUrlPass(mUrl + RESIZED_IMAGE);
                put(imageUrl, bitmap);
                if (mCacheCard) {
                    writeImageToCard(bitmap, imageUrl);
                }
            } catch (IOException e) {
                Log.d(Const.ERROR, e.getMessage(), e);
            } finally {
                if (inputStream != null) {
                    closeStream(inputStream);
                }
            }
            return resizedBitmap;
        }

        private void setOptions(BitmapFactory.Options options) {
            int origWidth = options.outWidth;
            int origHeight = options.outHeight;

            int scale;

            if (origWidth > origHeight) {
                scale = Math.round((float) origHeight / (float) mTargetHeight);
            } else {
                scale = Math.round((float) origWidth / (float) mTargetWidth);
            }

            options.inSampleSize = scale;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inJustDecodeBounds = false;
        }

        private void closeStream(InputStream inputStream) {
            try {
                inputStream.close();
            } catch (IOException e) {
                Log.d(Const.ERROR, e.getMessage(), e);
            }
        }

        @Nullable
        private Bitmap getImageExternalCache(String imageUrl) {
            if (isExternalStorageWritable()) {
                File file = new File(mContext.getExternalCacheDir(), imageUrl);
                if (file.exists()) {
                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    if (bitmap != null) {
                        put(imageUrl, bitmap);
                    }
                    return bitmap;
                }
            }
            return null;
        }

        private void writeImageToCard(Bitmap bitmap, String imageUrl) {
            if (isExternalStorageWritable()) {
                File file = new File(mContext.getExternalCacheDir(), imageUrl);
                FileOutputStream fileOutputStream;
                try {
                    fileOutputStream = new FileOutputStream(file);
                } catch (FileNotFoundException e) {
                    Log.d(Const.ERROR, e.getMessage());
                    return;
                }
                bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESS_QUALITY, fileOutputStream);
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (IOException e) {
                    Log.d(Const.ERROR, e.getMessage());
                }


            }
        }
    }
}
