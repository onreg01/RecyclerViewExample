package com.onregs.recyclerviewexample.data;

import android.provider.BaseColumns;

/**
 * Created by vadim on 16.11.2015.
 */
public class Feed {
    private String mId;
    private String mTitle;
    private String mDescription;
    private String mImageUrl;
    private int mRating;
    private String mLink;
    private long mCreatedAt;
    private String mFeedType;
    private int mCommentsCount;

    public static final String TYPE_EXTERNAL = "external";

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public void setImageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public int getRating() {
        return mRating;
    }

    public void setRating(int rating) {
        this.mRating = rating;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        this.mLink = link;
    }

    public long getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(long createdAt) {
        this.mCreatedAt = createdAt;
    }

    public String getFeedType() {
        return mFeedType;
    }

    public void setFeedType(String feedType) {
        this.mFeedType = feedType;
    }

    public int getCommentsCount() {
        return mCommentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.mCommentsCount = commentsCount;
    }

    public static abstract class FeedEntry implements BaseColumns {

        public static final String COLUMN_FEED_TITTLE = "title";
        public static final String COLUMN_FEED_DESCRIPTION = "description";
        public static final String COLUMN_IMAGE_URL = "image_url";
        public static final String COLUMN_FEED_RATING = "rating";
        public static final String COLUMN_FEED_CREATED_AT = "created_at";
        public static final String COLUMN_FEED_LINK = "link";
        public static final String COLUMN_FEED_TYPE = "type";
        public static final String COLUMN_FEED_COMMENTS_COUNT = "comments_count";
        public static final String COLUMN_FEED_CONTENT_TIME_STAMP = "content_time_stamp";

        public static final String TABLE_FEEDS = "feeds";

        public static final String CREATE_FEEDS_TABLE =
                "create table " + TABLE_FEEDS + "(" +
                        _ID + " text primary key, " +
                        COLUMN_FEED_TITTLE + " text, " +
                        COLUMN_FEED_DESCRIPTION + " text, " +
                        COLUMN_IMAGE_URL + " text, " +
                        COLUMN_FEED_RATING + " integer, " +
                        COLUMN_FEED_CREATED_AT + " long, " +
                        COLUMN_FEED_LINK + " text, " +
                        COLUMN_FEED_TYPE + " text, " +
                        COLUMN_FEED_COMMENTS_COUNT + " integer, " +
                        COLUMN_FEED_CONTENT_TIME_STAMP + " long " +
                        ");";
    }
}
