package com.onregs.recyclerviewexample.data;

import android.provider.BaseColumns;

/**
 * Created by vadim on 18.12.2015.
 */
public class FavoriteFeed {

    public static final String IS_FAVORITE = "is_favorite";
    public static final String NOT_FAVORITE = "0";

    public static abstract class FavoriteFeedEntry implements BaseColumns {

        public static final String COLUMN_USER_ID = "user_id";
        public static final String COLUMN_FEED_ID = "feed_id";
        public static final String COLUMN_CREATED_AT = "created_at";


        public static final String TABLE_FAVORITE_FEEDS = "favorite_feeds";

        public static final String CREATE_FAVORITE_FEEDS_TABLE =
                "create table " + TABLE_FAVORITE_FEEDS + "(" +
                        _ID + " integer primary key autoincrement, " +
                        COLUMN_USER_ID + " text, " +
                        COLUMN_FEED_ID + " text, " +
                        COLUMN_CREATED_AT + " long " +
                        ");";
    }
}
