package com.onregs.recyclerviewexample.data.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.onregs.recyclerviewexample.connection.ParseComConnection;
import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;
import com.onregs.recyclerviewexample.json.CustomJsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by vadim on 18.11.2015.
 */
public class FeedLoader extends AsyncTaskLoader<LoaderAnswer> {

    public FeedLoader(Context context) {
        super(context);
    }

    @Override
    public LoaderAnswer loadInBackground() {
        ArrayList<Feed> feeds = new ArrayList<>();
        InputStream feedStream;

        try {
            feedStream = ParseComConnection.getDataStream(ParseComConnection.URL_FEEDS, null);
        } catch (IOException e) {
            return new LoaderAnswer(false, LoaderAnswer.CONNECTION_ERROR, getContext());
        }
        try {
            new CustomJsonReader(feeds, CustomJsonReader.READ_FEEDS).fillData(feedStream);
        } catch (IOException e) {
            return new LoaderAnswer(false, LoaderAnswer.PARSE_DATA_ERROR, getContext());
        }

        if (feeds.size() == 0) {
            return new LoaderAnswer(false, LoaderAnswer.EMPTY_DATA, getContext());
        }
        DataSqliteOpenHelper dataSqliteOpenHelper = DataSqliteOpenHelper.getInstance(getContext());


        dataSqliteOpenHelper.addFeeds(feeds, getContext());
        return new LoaderAnswer(true, LoaderAnswer.SUCCESSFULLY, getContext());
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

}
