package com.onregs.recyclerviewexample.data.db.content_provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.onregs.recyclerviewexample.data.FavoriteFeed;
import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;

/**
 * Created by vadim on 23.12.2015.
 */
public class FeedContentProvider extends SimpleContentProvider {

    public static final String AUTHORITY = "com.onregs.providers.feed_provider";

    public static final String FEED_PASS = Feed.FeedEntry.TABLE_FEEDS;
    public static final String FEED_SUGGESTION_PASS = Feed.FeedEntry.TABLE_FEEDS + "/suggestions";

    public static final Uri CONTENT_URI_FEEDS = Uri.parse("content://" + AUTHORITY + "/" + FEED_PASS);
    public static final Uri CONTENT_URI_SUGGESTION_FEEDS = Uri.parse("content://" + AUTHORITY + "/" + FEED_SUGGESTION_PASS);

    static final int URI_FEEDS = 1;
    static final int URI_FEEDS_SUGGESTIONS = 2;

    private static final UriMatcher sUriMatcher;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, FEED_PASS, URI_FEEDS);
        sUriMatcher.addURI(AUTHORITY, FEED_SUGGESTION_PASS, URI_FEEDS_SUGGESTIONS);
    }

    public static final String TABLE_FEEDS_JOIN = Feed.FeedEntry.TABLE_FEEDS + " LEFT JOIN " +
            FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS + " ON " +
            Feed.FeedEntry.TABLE_FEEDS + "." +
            Feed.FeedEntry._ID + " = " +
            FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS + "." +
            FavoriteFeed.FavoriteFeedEntry.COLUMN_FEED_ID;
    
    public static final String[] FEED_PROJECTION =
            new String[]{"feeds.*", "sum(favorite_feeds.user_id = " +
                    DataSqliteOpenHelper.CustomQueryBuilder.SELECT_CURRENT_USER_ID + ") as is_favorite"};

    public static final String GROUP_BY = "feeds._id";


    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        int machUri = sUriMatcher.match(uri);
        Cursor newCursor = null;
        switch (machUri) {
            case URI_FEEDS:
                newCursor = mDataSqliteOpenHelper.getWritableDatabase().query(TABLE_FEEDS_JOIN, projection, selection, selectionArgs,
                        GROUP_BY, null, sortOrder);
                break;
            case URI_FEEDS_SUGGESTIONS:
                newCursor = mDataSqliteOpenHelper.getWritableDatabase().query(
                        true, Feed.FeedEntry.TABLE_FEEDS, projection, selection, selectionArgs, null, null, sortOrder, null);
                break;
            default:
                break;
        }

        Context context = getContext();
        if (context != null && newCursor != null) {
            if (machUri == URI_FEEDS) {
                newCursor.setNotificationUri(context.getContentResolver(), CONTENT_URI_FEEDS);
            }
        }
        return newCursor;
    }

    private DataSqliteOpenHelper mDataSqliteOpenHelper;

    @Override
    public boolean onCreate() {
        mDataSqliteOpenHelper = DataSqliteOpenHelper.getInstance(getContext());
        return true;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return mDataSqliteOpenHelper.getWritableDatabase().update(Feed.FeedEntry.TABLE_FEEDS, values, selection, selectionArgs);
    }
}
