package com.onregs.recyclerviewexample.data.db.content_provider;

import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.net.Uri;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.Feed;

import java.util.ArrayList;

/**
 * Created by vadim on 24.12.2015.
 */
public class SuggestionProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "com.onregs.providers.suggestion_provider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public static final String LIKE = " like ?";
    public static final String SHORT_TYPE = "ASC";
    public static final String ORDER_BY_TITTLE = Feed.FeedEntry.COLUMN_FEED_TITTLE + " " + SHORT_TYPE;
    public static final String ORDER_BY_DESCRIPTION = Feed.FeedEntry.COLUMN_FEED_DESCRIPTION + " " + SHORT_TYPE;

    public static final String FORMAT = "0";
    public static final String ID = "_ID";
    public static final Uri ICON_URI = Uri.parse("android.resource://com.onregs.recyclerviewexample/" + R.drawable.ic_search_hint);

    private String mQuery;

    private MergeCursor mMergeCursor;

    public SuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        mQuery = selectionArgs[0];

        Context context = getContext();
        if (context != null) {
            ContentResolver contentResolver = context.getContentResolver();

            Cursor dbCursorTittles = contentResolver.query(
                    FeedContentProvider.CONTENT_URI_SUGGESTION_FEEDS, new String[]{Feed.FeedEntry.COLUMN_FEED_TITTLE},
                    Feed.FeedEntry.COLUMN_FEED_TITTLE + LIKE, new String[]{"%" + mQuery + "%"}, ORDER_BY_TITTLE);

            Cursor dbCursorDescription = contentResolver.query(
                    FeedContentProvider.CONTENT_URI_SUGGESTION_FEEDS, new String[]{Feed.FeedEntry.COLUMN_FEED_DESCRIPTION},
                    Feed.FeedEntry.COLUMN_FEED_DESCRIPTION + LIKE, new String[]{"%" + mQuery + "%"}, ORDER_BY_DESCRIPTION);

            Cursor cacheCursor = super.query(uri, projection, selection, selectionArgs, sortOrder);

            mergeWithCashCursor(cacheCursor, dbCursorTittles, dbCursorDescription);
        }
        return mMergeCursor;
    }

    private void mergeWithCashCursor(Cursor cacheCursor, Cursor dbCursorTittles, Cursor dbCursorDescription) {

        ArrayList<String> suggestions = new ArrayList<>();

        MatrixCursor matrixCursor = new MatrixCursor(new String[]{SearchManager.SUGGEST_COLUMN_FORMAT,
                SearchManager.SUGGEST_COLUMN_ICON_1, SearchManager.SUGGEST_COLUMN_TEXT_1, SearchManager.SUGGEST_COLUMN_QUERY, ID});

        fillWordSuggestion(dbCursorTittles, Feed.FeedEntry.COLUMN_FEED_TITTLE, suggestions);
        fillWordSuggestion(dbCursorDescription, Feed.FeedEntry.COLUMN_FEED_DESCRIPTION, suggestions);
        fillMatrixCursor(matrixCursor, suggestions);
        mMergeCursor = new MergeCursor(new Cursor[]{cacheCursor, matrixCursor});
    }

    private void fillWordSuggestion(Cursor cursor, String column, ArrayList<String> suggestions) {
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String text = cursor.getString(cursor.getColumnIndex(column));
                    String[] words = text.split(" ");
                    for (String suggestion : words) {
                        if (suggestion.contains(mQuery)) {
                            if (!suggestions.contains(suggestion)) {
                                suggestions.add(suggestion);
                            }
                        }
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
    }

    private void fillMatrixCursor(MatrixCursor matrixCursor, ArrayList<String> suggestions) {
        for (String suggestion : suggestions) {
            matrixCursor.addRow(new Object[]{FORMAT, ICON_URI, suggestion, suggestion, null});
        }
    }
}
