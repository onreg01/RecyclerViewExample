package com.onregs.recyclerviewexample.data;

import android.provider.BaseColumns;


/**
 * Created by vadim on 17.12.2015.
 */
public class User {

    private String mId;
    private String mEmail;
    private String mUserName;
    private String mPassword;
    private String mAccessToken;
    private String mAccountName;
    private boolean mAuthorized;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mail) {
        this.mEmail = mail;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    public boolean isAuthorized() {
        return mAuthorized;
    }

    public void setAuthorized(boolean authorized) {
        this.mAuthorized = authorized;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        this.mAccessToken = accessToken;
    }

    public String getAccountName() {
        return mAccountName;
    }

    public void setAccountName(String accountName) {
        this.mAccountName = accountName;
    }

    public static abstract class UserEntry implements BaseColumns {

        public static final String COLUMN_USER_NAME = "user_name";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_PASSWORD = "password";
        public static final String COLUMN_ACCESS_TOKEN = "access_token";
        public static final String COLUMN_ACCOUNT_NAME = "account_name";

        public static final String TABLE_USERS = "users";

        public static final String CREATE_USERS_TABLE =
                "create table " + TABLE_USERS + "(" +
                        _ID + " text primary key, " +
                        COLUMN_USER_NAME + " text, " +
                        COLUMN_EMAIL + " text, " +
                        COLUMN_ACCESS_TOKEN + " text, " +
                        COLUMN_ACCOUNT_NAME + " text " +
                        ");";
    }
}
