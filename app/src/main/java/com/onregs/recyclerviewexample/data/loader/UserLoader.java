package com.onregs.recyclerviewexample.data.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.support.annotation.Nullable;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.connection.ParseComConnection;
import com.onregs.recyclerviewexample.data.User;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;
import com.onregs.recyclerviewexample.json.CustomJsonReader;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by vadim on 22.12.2015.
 */
public class UserLoader extends AsyncTaskLoader<UserLoader.UserLoaderAnswer> {

    private String mEmail;
    private String mPassword;
    private int mAction;

    public static final int ACTION_LOGIN = 0;
    public static final int ACTION_REGISTER = 1;

    public UserLoader(Context context, String email, @Nullable String password, int action) {
        super(context);
        this.mEmail = email;
        this.mAction = action;
        this.mPassword = password;
    }

    @Override
    public UserLoaderAnswer loadInBackground() {

        User user = new User();
        InputStream inputStream;
        try {
            inputStream = ParseComConnection.getUserStream(mEmail, mPassword);
        } catch (IOException e) {
            return new UserLoaderAnswer(false, UserLoaderAnswer.CONNECTION_ERROR, getContext());
        }
        try {
            new CustomJsonReader(user, CustomJsonReader.READ_USER).fillData(inputStream);
        } catch (IOException e) {
            return new UserLoaderAnswer(false, UserLoaderAnswer.PARSE_DATA_ERROR, getContext());
        }

        if (!user.isAuthorized()) {
            if (mAction == ACTION_REGISTER) {
                return new UserLoaderAnswer(true, UserLoaderAnswer.SUCCESSFULLY, getContext());
            }
            return new UserLoaderAnswer(false, UserLoaderAnswer.WRONG_LOGIN, getContext());
        }

        if (mAction == ACTION_REGISTER) {
            return new UserLoaderAnswer(false, UserLoaderAnswer.EMAIL_ALREADY_USE, getContext());
        }

        DataSqliteOpenHelper.getInstance(getContext()).addActiveUser(user);

        UserLoaderAnswer userLoaderAnswer = new UserLoaderAnswer(true, UserLoaderAnswer.SUCCESSFULLY, getContext());
        userLoaderAnswer.setUser(user);

        return userLoaderAnswer;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    public class UserLoaderAnswer extends LoaderAnswer {

        public static final int WRONG_LOGIN = 4;
        public static final int EMAIL_ALREADY_USE = 5;
        private User mUser;

        public UserLoaderAnswer(boolean result, int typeAnswer, Context context) {
            super(result, typeAnswer, context);
            this.setUserLoaderMessage(typeAnswer);
        }

        public void setUserLoaderMessage(int typeAnswer) {
            if (WRONG_LOGIN == typeAnswer) {
                mMessage = mContext.getString(R.string.wrong_login);
            } else if (EMAIL_ALREADY_USE == typeAnswer) {
                mMessage = mContext.getString(R.string.email_already_use);
            }
        }

        public User getUser() {
            return mUser;
        }

        public void setUser(User user) {
            this.mUser = user;
        }
    }
}


