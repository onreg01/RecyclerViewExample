package com.onregs.recyclerviewexample.data.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.onregs.recyclerviewexample.connection.ParseComConnection;
import com.onregs.recyclerviewexample.data.Comment;
import com.onregs.recyclerviewexample.data.MediaContent;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;
import com.onregs.recyclerviewexample.json.CustomJsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by vadim on 12.12.2015.
 */
public class ContentLoader extends AsyncTaskLoader<LoaderAnswer> {

    private String mId;

    public ContentLoader(Context context, String id) {
        super(context);
        this.mId = id;
    }

    @Override
    public LoaderAnswer loadInBackground() {

        ArrayList<MediaContent> mediaContents = new ArrayList<>();
        ArrayList<Comment> comments = new ArrayList<>();

        InputStream inputStreamMedia;
        InputStream inputStreamComments;
        try {
            inputStreamMedia = ParseComConnection.getDataStream(ParseComConnection.URL_MEDIA, mId);
            inputStreamComments = ParseComConnection.getDataStream(ParseComConnection.URL_COMMENTS, mId);
        } catch (IOException e) {
            return new LoaderAnswer(false, LoaderAnswer.CONNECTION_ERROR, getContext());
        }
        try {
            new CustomJsonReader(mediaContents, CustomJsonReader.READ_MEDIA_CONTENT).fillData(inputStreamMedia);
            new CustomJsonReader(comments, CustomJsonReader.READ_COMMENTS).fillData(inputStreamComments);
        } catch (IOException e) {
            return new LoaderAnswer(false, LoaderAnswer.PARSE_DATA_ERROR, getContext());
        }

        if (mediaContents.size() == 0) {
            return new LoaderAnswer(false, LoaderAnswer.EMPTY_DATA, getContext());
        }

        DataSqliteOpenHelper dataSqliteOpenHelper = DataSqliteOpenHelper.getInstance(getContext());

        dataSqliteOpenHelper.addMediaContent(mediaContents);
        dataSqliteOpenHelper.addComments(comments);
        dataSqliteOpenHelper.updateContentTimeStamp(comments.get(0).getFeedId());

        return new LoaderAnswer(true, LoaderAnswer.SUCCESSFULLY, getContext());
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
