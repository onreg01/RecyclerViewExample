package com.onregs.recyclerviewexample.data;

import android.provider.BaseColumns;

/**
 * Created by vadim on 12.12.2015.
 */
public class MediaContent {
    private String mId;
    private String mLink;
    private String mType;
    private int mFeedId;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        this.mLink = link;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public int getFeedId() {
        return mFeedId;
    }

    public void setFeedId(int feedId) {
        this.mFeedId = feedId;
    }

    public static abstract class MediaContentEntry implements BaseColumns {

        public static final String COLUMN_FIELD_LINK = "link";
        public static final String COLUMN_MEDIA_TYPE = "media_type";
        public static final String COLUMN_FEED_ID = "feed_id";


        public static final String TABLE_MEDIA_CONTENT = "media_content";

        public static final String CREATE_MEDIA_CONTENT_TABLE =
                "create table " + TABLE_MEDIA_CONTENT + "(" +
                        _ID + " text primary key, " +
                        COLUMN_FIELD_LINK + " text, " +
                        COLUMN_MEDIA_TYPE + " text, " +
                        COLUMN_FEED_ID + " integer " +
                        ");";
    }

}
