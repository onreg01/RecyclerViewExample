package com.onregs.recyclerviewexample.data;

import android.provider.BaseColumns;

/**
 * Created by vadim on 25.12.2015.
 */
public class ChangedFeed {

    public static abstract class ChangedFeedEntry implements BaseColumns {

        public static final String COLUMN_USER_ID = "user_id";
        public static final String COLUMN_FEED_ID = "feed_id";
        public static final String COLUMN_ACTION = "action";

        public static final String TABLE_CHANGED_FEEDS = "changed_feeds";

        public static final String CREATE_CHANGED_FEEDS_TABLE =
                "create table " + TABLE_CHANGED_FEEDS + "(" +
                        _ID + " integer primary key autoincrement, " +
                        COLUMN_USER_ID + " text, " +
                        COLUMN_FEED_ID + " text, " +
                        COLUMN_ACTION + " integer " +
                        ");";
    }
}

