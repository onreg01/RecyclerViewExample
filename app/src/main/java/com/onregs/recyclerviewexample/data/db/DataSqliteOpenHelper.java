package com.onregs.recyclerviewexample.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.onregs.recyclerviewexample.data.ChangedFeed;
import com.onregs.recyclerviewexample.data.Comment;
import com.onregs.recyclerviewexample.data.FavoriteFeed;
import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.MediaContent;
import com.onregs.recyclerviewexample.data.User;
import com.onregs.recyclerviewexample.utils.DateUtils;

import java.util.ArrayList;

/**
 * Created by vadim on 21.11.2015.
 */
public class DataSqliteOpenHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "com.onregs.recyclerviewexample.db";
    public static final int DB_VERSION = 6;
    public static final int OLD_DB_VERSION = 3;

    public DataSqliteOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    private static volatile DataSqliteOpenHelper sDataSqliteOpenHelper;

    public static DataSqliteOpenHelper getInstance(Context context) {
        if (sDataSqliteOpenHelper == null) {
            synchronized (DataSqliteOpenHelper.class) {
                if (sDataSqliteOpenHelper == null) {
                    sDataSqliteOpenHelper = new DataSqliteOpenHelper(context);
                }
            }
        }
        return sDataSqliteOpenHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Feed.FeedEntry.CREATE_FEEDS_TABLE);
        db.execSQL(MediaContent.MediaContentEntry.CREATE_MEDIA_CONTENT_TABLE);
        db.execSQL(Comment.CommentsContentEntry.CREATE_COMMENTS_TABLE);
        db.execSQL(User.UserEntry.CREATE_USERS_TABLE);
        db.execSQL(FavoriteFeed.FavoriteFeedEntry.CREATE_FAVORITE_FEEDS_TABLE);
        db.execSQL(ChangedFeed.ChangedFeedEntry.CREATE_CHANGED_FEEDS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < OLD_DB_VERSION) {
            db.execSQL("DROP TABLE IF EXISTS " + FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS);
            db.execSQL(FavoriteFeed.FavoriteFeedEntry.CREATE_FAVORITE_FEEDS_TABLE);
        }

        if (oldVersion <= OLD_DB_VERSION) {
            db.execSQL("DROP TABLE IF EXISTS " + User.UserEntry.TABLE_USERS);
            db.execSQL(User.UserEntry.CREATE_USERS_TABLE);
        }
    }

    public void addFeeds(ArrayList<Feed> feeds, Context context) {

        SQLiteDatabase sqLiteDatabase = sDataSqliteOpenHelper.getWritableDatabase();
        sqLiteDatabase.delete(Feed.FeedEntry.TABLE_FEEDS, null, null);

        sqLiteDatabase.beginTransaction();
        try {
            for (Feed feed : feeds) {
                ContentValues values = new ContentValues();
                values.put(Feed.FeedEntry._ID, feed.getId());
                values.put(Feed.FeedEntry.COLUMN_FEED_TITTLE, feed.getTitle());
                values.put(Feed.FeedEntry.COLUMN_FEED_DESCRIPTION, feed.getDescription());
                values.put(Feed.FeedEntry.COLUMN_IMAGE_URL, feed.getImageUrl());
                values.put(Feed.FeedEntry.COLUMN_FEED_RATING, feed.getRating());
                values.put(Feed.FeedEntry.COLUMN_FEED_CREATED_AT, feed.getCreatedAt());
                values.put(Feed.FeedEntry.COLUMN_FEED_LINK, feed.getLink());
                values.put(Feed.FeedEntry.COLUMN_FEED_TYPE, feed.getFeedType());
                values.put(Feed.FeedEntry.COLUMN_FEED_COMMENTS_COUNT, feed.getCommentsCount());
                sDataSqliteOpenHelper.getWritableDatabase().insert(Feed.FeedEntry.TABLE_FEEDS, null, values);
            }
            DateUtils.setTimeStamp(context);
            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }
    }

    public void addMediaContent(ArrayList<MediaContent> mediaContents) {
        SQLiteDatabase sqLiteDatabase = sDataSqliteOpenHelper.getWritableDatabase();
        sqLiteDatabase.delete(MediaContent.MediaContentEntry.TABLE_MEDIA_CONTENT,
                MediaContent.MediaContentEntry.COLUMN_FEED_ID + "=?", new String[]{String.valueOf(mediaContents.get(0).getFeedId())});

        sqLiteDatabase.beginTransaction();
        try {
            for (MediaContent mediaContent : mediaContents) {
                ContentValues values = new ContentValues();
                values.put(MediaContent.MediaContentEntry._ID, mediaContent.getId());
                values.put(MediaContent.MediaContentEntry.COLUMN_FIELD_LINK, mediaContent.getLink());
                values.put(MediaContent.MediaContentEntry.COLUMN_MEDIA_TYPE, mediaContent.getType());
                values.put(MediaContent.MediaContentEntry.COLUMN_FEED_ID, mediaContent.getFeedId());
                sDataSqliteOpenHelper.getWritableDatabase().insert(MediaContent.MediaContentEntry.TABLE_MEDIA_CONTENT, null, values);
            }
            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }
    }

    public void addComments(ArrayList<Comment> comments) {
        SQLiteDatabase sqLiteDatabase = sDataSqliteOpenHelper.getWritableDatabase();
        sqLiteDatabase.delete(Comment.CommentsContentEntry.TABLE_COMMENTS,
                Comment.CommentsContentEntry.COLUMN_FEED_ID + "=?", new String[]{String.valueOf(comments.get(0).getFeedId())});
        sqLiteDatabase.beginTransaction();
        try {
            for (Comment comment : comments) {
                ContentValues values = new ContentValues();
                values.put(Comment.CommentsContentEntry._ID, comment.getId());
                values.put(Comment.CommentsContentEntry.COLUMN_NAME, comment.getName());
                values.put(Comment.CommentsContentEntry.COLUMN_LAST_NAME, comment.getLastName());
                values.put(Comment.CommentsContentEntry.COLUMN_COMMENT, comment.getComment());
                values.put(Comment.CommentsContentEntry.COLUMN_COMMENT_CREATED_AT, comment.getCreatedAt());
                values.put(Comment.CommentsContentEntry.COLUMN_FEED_ID, comment.getFeedId());
                sDataSqliteOpenHelper.getWritableDatabase().insert(Comment.CommentsContentEntry.TABLE_COMMENTS, null, values);
            }
            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }
    }

    public void updateContentTimeStamp(String feedId) {
        ContentValues values = new ContentValues();
        values.put(Feed.FeedEntry.COLUMN_FEED_CONTENT_TIME_STAMP, System.currentTimeMillis() + DateUtils.FEED_CONTENT_TIME_STAMP);
        sDataSqliteOpenHelper.getWritableDatabase().update(
                Feed.FeedEntry.TABLE_FEEDS, values, Feed.FeedEntry._ID + "=?", new String[]{String.valueOf(feedId)});
    }


    public Cursor getFavoriteFeeds(String table, String[] selectionColumns, String selection, String[] selectionArgs, String sortBy) {
        return sDataSqliteOpenHelper.getReadableDatabase().query(table, selectionColumns, selection, selectionArgs, null, null, sortBy);
    }

    public Cursor getActiveUser() {
        return sDataSqliteOpenHelper.getReadableDatabase().query(User.UserEntry.TABLE_USERS, null, null, null, null, null, null);
    }

    public void addActiveUser(User user) {
        ContentValues values = new ContentValues();
        values.put(User.UserEntry._ID, user.getId());
        values.put(User.UserEntry.COLUMN_EMAIL, user.getEmail());
        values.put(User.UserEntry.COLUMN_USER_NAME, user.getUserName());
        values.put(User.UserEntry.COLUMN_ACCESS_TOKEN, user.getAccessToken());
        values.put(User.UserEntry.COLUMN_ACCOUNT_NAME, user.getAccountName());
        sDataSqliteOpenHelper.getWritableDatabase().insert(User.UserEntry.TABLE_USERS, null, values);
    }

    public void deleteActiveUser() {
        sDataSqliteOpenHelper.getWritableDatabase().delete(User.UserEntry.TABLE_USERS, null, null);
    }

    public void updateUserData(String accessToken, String accountName) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(User.UserEntry.COLUMN_ACCESS_TOKEN, accessToken);
        contentValues.put(User.UserEntry.COLUMN_ACCOUNT_NAME, accountName);
        sDataSqliteOpenHelper.getWritableDatabase().update(
                User.UserEntry.TABLE_USERS, contentValues, User.UserEntry._ID + " = " + CustomQueryBuilder.SELECT_CURRENT_USER_ID, null);
    }

    public static class CustomQueryBuilder {

        public static final String SELECT_CURRENT_USER_ID = "(SELECT " + User.UserEntry._ID + " FROM " + User.UserEntry.TABLE_USERS + ")";

        public static String getInsertQueryWithSelection(String table, String nullColumnHack, ContentValues values) {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT");
            sql.append(" INTO ");
            sql.append(table);
            sql.append("(");

            Object[] bindArgs;
            int size = (values != null && values.size() > 0) ? values.size() : 0;

            if (size > 0) {
                int i = 0;
                bindArgs = new Object[size];
                for (String columnName : values.keySet()) {
                    sql.append((i > 0) ? "," : "");
                    sql.append(columnName);
                    bindArgs[i++] = values.get(columnName);
                }
                sql.append(")");
                sql.append(" VALUES (");

                for (i = 0; i < size; i++) {
                    sql.append((i > 0) ? "," : "");
                    sql.append(bindArgs[i]);
                }
            } else {
                sql.append(nullColumnHack);
                sql.append(") VALUES (NULL");
            }
            sql.append(")");

            return sql.toString();
        }
    }
}
