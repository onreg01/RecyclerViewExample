package com.onregs.recyclerviewexample.data.db.content_provider.async_query_handler;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.onregs.recyclerviewexample.data.FavoriteFeed;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;
import com.onregs.recyclerviewexample.data.db.content_provider.DataContentProvider;

import java.lang.ref.WeakReference;

/**
 * Created by vadim on 23.12.2015.
 */
public class CustomAsyncQueryHandler extends AsyncQueryHandler {

    private WeakReference<AsyncQueryListener> mQueryListener;
    private WeakReference<AsyncListener> mListener;

    public static final int ASYNC_TOKEN = 0;

    public static final String DELETE_FAVORITE_FEED_SELECTION = "(" + FavoriteFeed.FavoriteFeedEntry.COLUMN_USER_ID + " =" +
            DataSqliteOpenHelper.CustomQueryBuilder.SELECT_CURRENT_USER_ID + ") AND " +
            FavoriteFeed.FavoriteFeedEntry.COLUMN_FEED_ID + " = ?";

    public CustomAsyncQueryHandler(ContentResolver contentResolver) {
        super(contentResolver);
    }

    @Override
    protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
        if (mQueryListener != null) {
            AsyncQueryListener asyncQueryListener = mQueryListener.get();
            if (asyncQueryListener != null) {
                asyncQueryListener.onQueryComplete(token, cookie, cursor);
            }
        }
    }

    public void addFeedToFavorite(String feedId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FavoriteFeed.FavoriteFeedEntry.COLUMN_FEED_ID, feedId);
        contentValues.put(FavoriteFeed.FavoriteFeedEntry.COLUMN_USER_ID, DataSqliteOpenHelper.CustomQueryBuilder.SELECT_CURRENT_USER_ID);
        contentValues.put(FavoriteFeed.FavoriteFeedEntry.COLUMN_CREATED_AT, System.currentTimeMillis());
        startInsert(ASYNC_TOKEN, null, DataContentProvider.CONTENT_URI_FAVORITES, contentValues);
    }

    public void deleteFeedFromFavorite(String feedId) {
        startDelete(ASYNC_TOKEN, null, DataContentProvider.CONTENT_URI_FAVORITES,
                DELETE_FAVORITE_FEED_SELECTION, new String[]{feedId});
    }

    @Override
    protected void onInsertComplete(int token, Object cookie, Uri uri) {
        onComplete();
    }

    @Override
    protected void onUpdateComplete(int token, Object cookie, int result) {
        onComplete();
    }

    @Override
    protected void onDeleteComplete(int token, Object cookie, int result) {
        onComplete();
    }

    public CustomAsyncQueryHandler setQueryListener(AsyncQueryListener asyncQueryListener) {
        mQueryListener = new WeakReference<>(asyncQueryListener);
        return this;
    }

    public CustomAsyncQueryHandler setListener(AsyncListener asyncListener) {
        mListener = new WeakReference<>(asyncListener);
        return this;
    }

    public void onComplete() {
        if (mListener != null) {
            AsyncListener asyncListener = mListener.get();
            if (asyncListener != null) {
                asyncListener.onComplete();
            }
        }
    }

    public interface AsyncQueryListener {
        void onQueryComplete(int token, Object cookie, Cursor cursor);
    }

    public interface AsyncListener {
        void onComplete();
    }
}
