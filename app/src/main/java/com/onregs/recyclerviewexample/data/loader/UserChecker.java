package com.onregs.recyclerviewexample.data.loader;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;

import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;

/**
 * Created by vadim on 17.12.2015.
 */
public class UserChecker extends CursorLoader {

    public UserChecker(Context context) {
        super(context);
    }

    @Override
    public Cursor loadInBackground() {

        DataSqliteOpenHelper sqLiteDatabase = DataSqliteOpenHelper.getInstance(getContext());
        return sqLiteDatabase.getActiveUser();
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
