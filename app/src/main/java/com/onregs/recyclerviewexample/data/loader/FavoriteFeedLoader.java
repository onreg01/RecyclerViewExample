package com.onregs.recyclerviewexample.data.loader;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;

import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;

/**
 * Created by vadim on 18.12.2015.
 */
public class FavoriteFeedLoader extends CursorLoader {
    private String mSelection;
    private String[] mSelectionArgs;
    private String mTable;
    private String[] mSelectionColumns;
    private String mSortBy;

    public FavoriteFeedLoader(Context context, String table, String[] selectionColumns, String selection,
                              String[] selectionArgs, String sortBy) {
        super(context);
        this.mTable = table;
        this.mSelection = selection;
        this.mSelectionArgs = selectionArgs;
        this.mSelectionColumns = selectionColumns;
        this.mSortBy = sortBy;
    }

    @Override
    public Cursor loadInBackground() {
        return DataSqliteOpenHelper.getInstance(getContext()).getFavoriteFeeds(mTable, mSelectionColumns,
                mSelection, mSelectionArgs, mSortBy);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
