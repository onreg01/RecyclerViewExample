package com.onregs.recyclerviewexample.utils;

import android.content.Context;

import com.onregs.recyclerviewexample.settings.Settings;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by vadim on 21.11.2015.
 */
public class DateUtils {

    public static final String ALL_TIME = "all_time";

    public static final String PERIOD_HOUR = "1";
    public static final String PERIOD_DAY = "2";
    public static final String PERIOD_WEEK = "3";
    public static final String PERIOD_MONTH = "4";
    public static final String PERIOD_YEAR = "5";
    public static final String PERIOD_ALL = "6";

    public static final int TIME_STAMP = 20 * 60 * 1000; //20 min
    public static final int FEED_CONTENT_TIME_STAMP = 5 * 60 * 1000; //5 min
    public static final int BACK_PERIOD = -1;

    public static final String DATE_FORMAT = "EEE, dd MMM yyyy HH:mm";

    public static String getPeriod(Context context) {
        Calendar calendar = Calendar.getInstance();
        String period = Settings.getInstance(context).getPeriod();
        switch (period) {
            case PERIOD_HOUR:
                calendar.add(Calendar.HOUR, BACK_PERIOD);
                break;
            case PERIOD_DAY:
                calendar.add(Calendar.DAY_OF_MONTH, BACK_PERIOD);
                break;
            case PERIOD_WEEK:
                calendar.add(Calendar.WEEK_OF_MONTH, BACK_PERIOD);
                break;
            case PERIOD_MONTH:
                calendar.add(Calendar.MONTH, BACK_PERIOD);
                break;
            case PERIOD_YEAR:
                calendar.add(Calendar.YEAR, BACK_PERIOD);
                break;
            case PERIOD_ALL:
                return ALL_TIME;
        }
        return String.valueOf(calendar.getTimeInMillis());
    }

    public static void setTimeStamp(Context context) {
        Settings.getInstance(context).setTimeStamp(System.currentTimeMillis() + TIME_STAMP);
    }

    public static boolean checkTimeStamp(Context context) {

        long currentTime = System.currentTimeMillis();
        long timeStamp = Settings.getInstance(context).getTimeStamp();

        return currentTime > timeStamp;
    }

    public static boolean checkFeedTimeStamp(long timeStamp) {

        long currentTime = System.currentTimeMillis();

        return currentTime > timeStamp;
    }

    public static String getStrData(long data) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        return simpleDateFormat.format(new Date(data));
    }
}
