package com.onregs.recyclerviewexample.utils;

import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.ImageView;

/**
 * Created by vadim on 18.12.2015.
 */
public class ImageSwitcher {

    public static final int TYPE_STANDARD = 0;
    public static final int TYPE_SECOND = 1;

    @Nullable
    private MenuItem mMenuItem;
    @Nullable
    private ImageView mImageView;

    private int mImage;
    private int mSecondImage;
    private boolean mDefaultState;

    private int mImageType;


    public ImageSwitcher(MenuItem menuItem, int image, int secondImage, boolean state) {
        this.mMenuItem = menuItem;
        this.mImage = image;
        this.mSecondImage = secondImage;
        this.mDefaultState = state;
        this.init();
    }

    public ImageSwitcher(ImageView imageView, int image, int secondImage, boolean state) {
        this.mImageView = imageView;
        this.mImage = image;
        this.mSecondImage = secondImage;
        this.mDefaultState = state;
        this.init();
    }

    private void init() {
        if (mDefaultState) {
            mImageType = TYPE_SECOND;
            if (checkMenuItem()) {
                mMenuItem.setIcon(mSecondImage);
            } else if (checkImageView()) {
                mImageView.setImageResource(mSecondImage);
            }
        } else {
            mImageType = TYPE_STANDARD;
            if (checkMenuItem()) {
                mMenuItem.setIcon(mImage);
            } else if (checkImageView()) {
                mImageView.setImageResource(mImage);
            }
        }
    }

    public boolean onAction() {
        if (mImageType == TYPE_STANDARD) {
            mImageType = TYPE_SECOND;

            if (checkMenuItem()) {
                mMenuItem.setIcon(mSecondImage);
            } else if (checkImageView()) {
                mImageView.setImageResource(mSecondImage);
            }
            return true;
        } else {
            mImageType = TYPE_STANDARD;
            if (checkMenuItem()) {
                mMenuItem.setIcon(mImage);
            } else if (checkImageView()) {
                mImageView.setImageResource(mImage);
            }
            return false;
        }
    }

    public boolean checkMenuItem() {
        return mMenuItem != null;
    }

    public boolean checkImageView() {
        return mImageView != null;
    }
}
