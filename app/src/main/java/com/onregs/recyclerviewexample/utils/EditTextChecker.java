package com.onregs.recyclerviewexample.utils;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Patterns;

import com.onregs.recyclerviewexample.R;

/**
 * Created by vadim on 17.12.2015.
 */
public class EditTextChecker {

    public static final int PASSWORD_SYMBOLS_COUNT = 6;

    public static boolean checkEmailField(Context context, TextInputLayout label, Editable text) {
        if (!checkIfEmpty(context, label, text)) {
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(text.toString()).matches()) {
            label.setError(context.getString(R.string.incorrect_mail));
            return false;
        }
        return true;
    }

    public static boolean checkIfEmpty(Context context, TextInputLayout label, Editable text) {
        if (TextUtils.isEmpty(text)) {
            label.setError(context.getString(R.string.empty_field));
            return false;
        }
        return true;
    }

    public static boolean checkPasswords(Context context, TextInputLayout labelPassword,
                                         TextInputLayout secondLabelPassword, Editable password, Editable secondPassword) {

        if (!checkIfEmpty(context, labelPassword, password)) {
            return false;
        }
        if (!checkIfEmpty(context, secondLabelPassword, secondPassword)) {
            return false;
        }
        if (password.length() < PASSWORD_SYMBOLS_COUNT) {
            labelPassword.setError(context.getString(R.string.short_password));
            return false;
        }
        if (secondPassword.length() < PASSWORD_SYMBOLS_COUNT) {
            secondLabelPassword.setError(context.getString(R.string.short_password));
            return false;
        }
        if (!(password.toString().equals(secondPassword.toString()))) {
            secondLabelPassword.setError(context.getString(R.string.passwords_do_not_match));
            return false;
        }
        return true;
    }

}
