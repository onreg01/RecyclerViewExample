package com.onregs.recyclerviewexample.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.onregs.recyclerviewexample.service.DataLoadService;
import com.onregs.recyclerviewexample.utils.InternetUtils;

public class CheckNetworkReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (InternetUtils.checkNetwork(context)) {
            context.startService(new Intent(context, DataLoadService.class));
        }
    }
}
