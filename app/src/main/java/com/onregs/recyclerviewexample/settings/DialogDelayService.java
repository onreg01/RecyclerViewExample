package com.onregs.recyclerviewexample.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.app.DefaultApplication;

public class DialogDelayService extends DialogPreference {

    public static final int DEFAULT_MIN_VALUE = 1;
    public static final int DEFAULT_MAX_VALUE = 360;

    private TextView current;

    private int minValue;
    private int maxValue;
    private int currentValue;

    public DialogDelayService(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public DialogDelayService(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DialogDelayService, 0, 0);
        try {
            minValue = typedArray.getInteger(R.styleable.DialogDelayService_minValue, DEFAULT_MIN_VALUE);
            maxValue = typedArray.getInteger(R.styleable.DialogDelayService_maxValue, DEFAULT_MAX_VALUE);
            if (minValue < 0 || maxValue < 0 || minValue > maxValue) {
                throw new IllegalArgumentException("min and max attributes most be > 0 and min < max");
            }
        } finally {
            typedArray.recycle();
        }
    }

    @Override
    protected View onCreateDialogView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.preference_custom_dialog_delay_service, null);
        current = (TextView) view.findViewById(R.id.current);
        currentValue = getPersistedInt(DEFAULT_MIN_VALUE);
        current.setText(DefaultApplication.getApp().getString(R.string.update_service_delay_current_value, currentValue));
        ((TextView) view.findViewById(R.id.min)).setText(String.valueOf(minValue));
        ((TextView) view.findViewById(R.id.max)).setText(String.valueOf(maxValue));

        setSeekBar(view);
        return view;
    }

    private void setSeekBar(View view) {
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        seekBar.setMax(maxValue - minValue);
        seekBar.setProgress(currentValue - minValue);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentValue = progress + minValue;
                current.setText(DefaultApplication.getApp().getString(R.string.update_service_delay_current_value, currentValue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (!positiveResult) {
            return;
        }
        persistInt(currentValue);
        notifyChanged();
    }

    @Override
    public CharSequence getSummary() {
        String summary = super.getSummary().toString();
        int value = getPersistedInt(DEFAULT_MIN_VALUE);
        return String.format(summary, value);
    }
}
