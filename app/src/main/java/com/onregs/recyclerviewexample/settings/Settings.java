package com.onregs.recyclerviewexample.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by vadim on 21.11.2015.
 */
public class Settings {

    private static volatile Settings sSettings;

    public static final String KEY_PERIOD = "pref_periods";
    public static final String KEY_NOTIFICATIONS = "pref_notifications";
    public static final String KEY_UPDATE_SERVICE_DELAY = "pref_update_service_delay";
    public static final String KEY_UPDATE_SERVICE_STATE = "pref_update_service_state";
    public static final String KEY_LOCATION_MANAGER_TYPE = "pref_location_manager";
    public static final String TIME_STAMP = "time_stamp";
    public static final String DEFAULT_PERIOD = "6";
    public static final String LAST_TAB_POSITION = "last_tab_position";
    public static final int DEFAULT_TAB_POSITION = 0;
    public static final String DEFAULT_LOCATION_MANAGER_TYPE = "network";
    public static final int DEFAULT_TIME_STAMP = 1;
    public static final boolean DEFAULT_NOTIFICATIONS_SETTINGS = false;
    public static final boolean DEFAULT_UPDATE_SERVICE_STATE = false;
    public static final int DEFAULT_UPDATE_SERVICE_DELAY = 1;

    private SharedPreferences mSharedPreferences;

    private Settings(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static Settings getInstance(Context context) {
        if (sSettings == null) {
            synchronized (Settings.class) {
                sSettings = new Settings(context);
            }
        }
        return sSettings;
    }

    public String getLocationMangerType()
    {
        return mSharedPreferences.getString(KEY_LOCATION_MANAGER_TYPE, DEFAULT_LOCATION_MANAGER_TYPE);
    }

    public String getPeriod() {
        return mSharedPreferences.getString(KEY_PERIOD, DEFAULT_PERIOD);
    }

    public boolean getNotificationsSettings() {
        return mSharedPreferences.getBoolean(KEY_NOTIFICATIONS, DEFAULT_NOTIFICATIONS_SETTINGS);
    }

    public void setLastTabPosition(int position) {
        mSharedPreferences.edit().putInt(LAST_TAB_POSITION, position).apply();
    }

    public int getLastTabPosition() {
        return mSharedPreferences.getInt(LAST_TAB_POSITION, DEFAULT_TAB_POSITION);
    }

    public void setTimeStamp(long timeStamp) {
        mSharedPreferences.edit().putLong(TIME_STAMP, timeStamp).apply();
    }

    public long getTimeStamp() {
        return mSharedPreferences.getLong(TIME_STAMP, DEFAULT_TIME_STAMP);
    }

    public int getUpdateServiceDelay()
    {
        return mSharedPreferences.getInt(KEY_UPDATE_SERVICE_DELAY, DEFAULT_UPDATE_SERVICE_DELAY);
    }

    public boolean getUpdateServiceState()
    {
        return mSharedPreferences.getBoolean(KEY_UPDATE_SERVICE_STATE, DEFAULT_UPDATE_SERVICE_STATE);
    }
}
