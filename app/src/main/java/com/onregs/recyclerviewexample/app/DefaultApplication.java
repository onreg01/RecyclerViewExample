package com.onregs.recyclerviewexample.app;

import android.app.Application;
import android.content.Intent;

import com.onregs.recyclerviewexample.service.MoveService;
import com.parse.Parse;

/**
 * Created by vadim on 23.12.2015.
 */
public class DefaultApplication extends Application {

    private static DefaultApplication INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;

        Parse.enableLocalDatastore(this);
        Parse.initialize(this);
    }

    public static Application getApp()
    {
        return INSTANCE;
    }
}
