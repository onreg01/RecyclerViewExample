package com.onregs.recyclerviewexample.service;

import android.Manifest;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.media.RingtoneManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.settings.Settings;
import com.onregs.recyclerviewexample.ui.activity.MoveCheckActivity;

public class MoveService extends Service implements LocationListener {

    public static final int MIN_UPDATE_TIME = 1000 * 60; //1 min
    public static final int MIN_UPDATE_DISTANCE = 10; //10 meters

    public static final int FOREGROUND_ID = 5;

    private final IBinder binder = new MoveServiceBinder();
    private LocationManager locManager;

    private String currentType;
    private double distance;

    private Location oldLocation;

    @Override
    public void onCreate() {
        super.onCreate();
        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        currentType = Settings.getInstance(getApplicationContext()).getLocationMangerType();
        initRequestLocationUpdates(currentType.equals(LocationManager.NETWORK_PROVIDER) ?
                LocationManager.NETWORK_PROVIDER : LocationManager.GPS_PROVIDER);

        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_directions_run_black_48dp)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("MoveService is working!")
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .build();

        startForeground (FOREGROUND_ID, notification);
    }

    public void initRequestLocationUpdates(String type) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager.requestLocationUpdates(type, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE, this);
        sendBroadcast(locManager.getLastKnownLocation(currentType));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class MoveServiceBinder extends Binder {
        public MoveService getMoveService() {
            return MoveService.this;
        }
    }

    public double getDistance() {

        return distance;
    }

    public void resetDistance() {
        distance = 0;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (oldLocation != null) {
            distance += oldLocation.distanceTo(location);
        }
        oldLocation = location;
        sendBroadcast(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void sendBroadcast(Location location) {
        if (location != null) {
            Intent intent = new Intent(MoveCheckActivity.LocationReceiver.BROADCAST_ACTION);
            intent.putExtra(MoveCheckActivity.LocationReceiver.LATITUDE, location.getLatitude());
            intent.putExtra(MoveCheckActivity.LocationReceiver.LONGITUDE, location.getLongitude());
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

}
