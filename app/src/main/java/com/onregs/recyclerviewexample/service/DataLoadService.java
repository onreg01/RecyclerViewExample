package com.onregs.recyclerviewexample.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.RemoteViews;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.loader.FeedLoader;
import com.onregs.recyclerviewexample.data.loader.LoaderAnswer;
import com.onregs.recyclerviewexample.settings.Settings;
import com.onregs.recyclerviewexample.ui.activity.FeedActivity;
import com.onregs.recyclerviewexample.utils.InternetUtils;


public class DataLoadService extends IntentService {

    public static final int REPEAT_SERVICE = 1;
    public static final int NOTIFY_ID = 123;

    public DataLoadService() {
        super(DataLoadService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (Settings.getInstance(getApplicationContext()).getUpdateServiceState()) {
            if (InternetUtils.checkNetwork(getApplicationContext())) {
                LoaderAnswer loaderAnswer = new FeedLoader(getApplicationContext()).loadInBackground();
                if (Settings.getInstance(getApplicationContext()).getNotificationsSettings()) {
                    sendNotification(loaderAnswer);
                }
                repeatService();
            }
        }
    }

    private void sendNotification(LoaderAnswer loaderAnswer) {
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notifications_description, loaderAnswer.getMessage()))
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(createIntent())
//                .setOngoing(true)
//                .setSound(Uri.parse("android.resource://"
//                        + getApplicationContext().getPackageName() + "/"
//                        + R.raw.muz))
                .build();

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFY_ID, notification);
    }

    private PendingIntent createIntent()
    {
        Intent intent = new Intent(this, FeedActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
       return PendingIntent.getActivity(this, REPEAT_SERVICE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void repeatService() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getService(this, REPEAT_SERVICE, new Intent(this, DataLoadService.class), PendingIntent.FLAG_UPDATE_CURRENT);

        long delayTime = Settings.getInstance(getApplicationContext()).getUpdateServiceDelay() * 60 * 1000;
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delayTime, pendingIntent);
    }
}
