package com.onregs.recyclerviewexample.listener;

import android.support.design.widget.TabLayout;

/**
 * Created by vadim on 22.11.2015.
 */
public abstract class CustomTabListener implements TabLayout.OnTabSelectedListener {

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
