package com.onregs.recyclerviewexample.listener;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.view.View;
import android.widget.Toast;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.connection.ParseComConnection;
import com.onregs.recyclerviewexample.data.ChangedFeed;
import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;
import com.onregs.recyclerviewexample.data.db.content_provider.DataContentProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.FeedContentProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.async_query_handler.CustomAsyncQueryHandler;
import com.onregs.recyclerviewexample.ui.fragment.dialog.CancelUpdateRatingDialog;
import com.onregs.recyclerviewexample.ui.fragment.dialog.UpdateRatingDialog;
import com.onregs.recyclerviewexample.utils.InternetUtils;

/**
 * Created by vadim on 24.12.2015.
 */
public class RatingChanger implements View.OnClickListener {

    public static final int IS_CHANGED = 0;

    public static final int ADD_TO_CHANGED = 1;
    public static final int DELETE_FROM_CHANGED = 2;
    public static final int UPDATE_FEED_RATING = 7;

    public static final String SELECTION = ChangedFeed.ChangedFeedEntry.COLUMN_FEED_ID + " = ? and " +
            ChangedFeed.ChangedFeedEntry.COLUMN_USER_ID + " = " +
            DataSqliteOpenHelper.CustomQueryBuilder.SELECT_CURRENT_USER_ID;

    public static final String UPDATE_RATING_SELECTION = Feed.FeedEntry._ID + " = ?";

    public static final int ACTION_INCREASE_RATING = 3;
    public static final int ACTION_DECREASE_RATING = 4;

    public static final int CANCEL_ACTION = 5;
    public static final int UPDATE_ACTION = 6;

    private int mUserAction;
    private String mFeedId;
    private int mRating;

    private Activity mActivity;
    private RatingChangerCallbacks mCallbacks;

    public RatingChanger(Activity activity, String feedId, int rating, RatingChangerCallbacks callbacks) {
        this.mFeedId = feedId;
        this.mRating = rating;
        this.mActivity = activity;
        this.mCallbacks = callbacks;
    }

    @Override
    public void onClick(View v) {
        new CustomAsyncQueryHandler(mActivity.getContentResolver()).setQueryListener(asyncQueryListener).startQuery(
                IS_CHANGED, null, DataContentProvider.CONTENT_URI_CHANGED_FEEDS, null, SELECTION,
                new String[]{mFeedId}, null);
    }


    private void openCancelUpdateRatingDialog() {
        CancelUpdateRatingDialog cancelUpdateRatingDialog = new CancelUpdateRatingDialog();
        cancelUpdateRatingDialog.setCallbacks(cancelUpdateDialog);
        cancelUpdateRatingDialog.show(mActivity.getFragmentManager(), CancelUpdateRatingDialog.class.getSimpleName());
    }

    private void openUpdateRatingDialog() {
        UpdateRatingDialog updateRatingDialog = new UpdateRatingDialog();
        updateRatingDialog.setCallbacks(updateRatingDialogCallbacks);
        updateRatingDialog.show(mActivity.getFragmentManager(), UpdateRatingDialog.class.getSimpleName());
    }


    private void connectionProblem() {
        Toast.makeText(mActivity, mActivity.getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
    }

    public CustomAsyncQueryHandler.AsyncQueryListener asyncQueryListener = new CustomAsyncQueryHandler.AsyncQueryListener() {
        @Override
        public void onQueryComplete(int token, Object cookie, Cursor cursor) {
            if (cursor.moveToFirst()) {
                mUserAction = cursor.getInt(cursor.getColumnIndex(ChangedFeed.ChangedFeedEntry.COLUMN_ACTION));
                openCancelUpdateRatingDialog();
            } else {
                openUpdateRatingDialog();
            }
            cursor.close();
        }
    };


    public UpdateRatingDialog.UpdateRatingDialogCallbacks
            updateRatingDialogCallbacks = new UpdateRatingDialog.UpdateRatingDialogCallbacks() {

        @Override
        public void increaseRating() {
            if (InternetUtils.checkNetwork(mActivity)) {
                mUserAction = ACTION_INCREASE_RATING;
                mRating++;
                ParseComConnection.updateRating(UPDATE_ACTION, mFeedId, mRating, parseConnectionCallbacks);
                mCallbacks.startPerform();
            } else {
                connectionProblem();
            }
        }

        @Override
        public void decreaseRating() {
            if (InternetUtils.checkNetwork(mActivity)) {
                mUserAction = ACTION_DECREASE_RATING;
                mRating--;
                ParseComConnection.updateRating(UPDATE_ACTION, mFeedId, mRating, parseConnectionCallbacks);
                mCallbacks.startPerform();
            } else {
                connectionProblem();
            }
        }
    };

    public CustomAsyncQueryHandler.AsyncListener asyncListener = new CustomAsyncQueryHandler.AsyncListener() {

        @Override
        public void onComplete() {
            mCallbacks.endPerform();
            mActivity.getContentResolver().notifyChange(FeedContentProvider.CONTENT_URI_FEEDS, null);
        }

    };

    public CancelUpdateRatingDialog.CancelUpdateRatingDialogCallbacks
            cancelUpdateDialog = new CancelUpdateRatingDialog.CancelUpdateRatingDialogCallbacks() {

        @Override
        public void onCancelUpdateRating() {
            if (InternetUtils.checkNetwork(mActivity)) {
                if (mUserAction == ACTION_INCREASE_RATING) {
                    mRating--;
                } else {
                    mRating++;
                }
                ParseComConnection.updateRating(CANCEL_ACTION, mFeedId, mRating, parseConnectionCallbacks);
                mCallbacks.startPerform();
            } else {
                connectionProblem();
            }
        }
    };

    public ParseComConnection.ConnectionCallbacks parseConnectionCallbacks = new ParseComConnection.ConnectionCallbacks() {

        @Override
        public void successfully(int token) {

            Toast.makeText(mActivity, mActivity.getString(R.string.successfully), Toast.LENGTH_SHORT).show();

            if (token == UPDATE_ACTION) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(ChangedFeed.ChangedFeedEntry.COLUMN_FEED_ID, mFeedId);
                contentValues.put(ChangedFeed.ChangedFeedEntry.COLUMN_USER_ID,
                        DataSqliteOpenHelper.CustomQueryBuilder.SELECT_CURRENT_USER_ID);

                contentValues.put(ChangedFeed.ChangedFeedEntry.COLUMN_ACTION, mUserAction);
                new CustomAsyncQueryHandler(mActivity.getContentResolver()).startInsert(ADD_TO_CHANGED, null,
                        DataContentProvider.CONTENT_URI_CHANGED_FEEDS, contentValues);
            } else if (token == CANCEL_ACTION) {
                new CustomAsyncQueryHandler(mActivity.getContentResolver()).startDelete(
                        DELETE_FROM_CHANGED, null, DataContentProvider.CONTENT_URI_CHANGED_FEEDS, SELECTION,
                        new String[]{mFeedId});
            }

            ContentValues contentValues = new ContentValues();
            contentValues.put(Feed.FeedEntry.COLUMN_FEED_RATING, mRating);
            new CustomAsyncQueryHandler(mActivity.getContentResolver()).setListener(asyncListener).startUpdate(UPDATE_FEED_RATING, null,
                    FeedContentProvider.CONTENT_URI_FEEDS, contentValues, UPDATE_RATING_SELECTION, new String[]{mFeedId});
        }

        @Override
        public void error(int errorToken) {
            if (errorToken == ParseComConnection.ERROR_UPDATE_RATING) {
                mCallbacks.endPerform();
                connectionProblem();
                Toast.makeText(mActivity, mActivity.getString(R.string.update_rating_error), Toast.LENGTH_SHORT).show();
            }
        }
    };

    public interface RatingChangerCallbacks {
        void startPerform();

        void endPerform();
    }
}
