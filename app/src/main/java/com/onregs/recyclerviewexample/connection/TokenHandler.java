package com.onregs.recyclerviewexample.connection;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

/**
 * Created by vadim on 08.01.2016.
 */
public class TokenHandler {

    public static final String ACCOUNT_TYPE = "com.google";
    public static final String TOKEN_TYPE = "ah";

    private Activity mActivity;
    private TokenHandlerCallbacks mAccessTokenCallbacks;
    private String mAccountName;

    public TokenHandler(Activity activity, String accountName, TokenHandlerCallbacks accessTokenCallbacks) {
        this.mActivity = activity;
        this.mAccessTokenCallbacks = accessTokenCallbacks;
        this.mAccountName = accountName;
    }

    public void getAccessToken() throws OperationCanceledException {
        AccountManager accountManager = AccountManager.get(mActivity);
        Account account = getAccountByName(accountManager.getAccountsByType(ACCOUNT_TYPE), mAccountName);

        if (account != null) {
            accountManager.getAuthToken(
                    account,
                    TOKEN_TYPE,
                    null,
                    mActivity,
                    mAccessTokenCallbacks,
                    new android.os.Handler(mAccessTokenCallbacks));
        } else {
            throw new OperationCanceledException("account not found");
        }
    }

    @Nullable
    public Account getAccountByName(Account[] accounts, String accountName) {
        for (Account account : accounts) {
            if (account.name.equals(accountName)) {
                return account;
            }
        }
        return null;
    }

    public interface TokenHandlerCallbacks extends AccountManagerCallback<Bundle>, Handler.Callback {

    }
}
