package com.onregs.recyclerviewexample.connection;

import android.support.annotation.Nullable;

import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.User;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by vadim on 18.11.2015.
 */
public class ParseComConnection {

    public static final String URL_FEEDS = "https://api.parse.com/1/classes/data";
    public static final String URL_MEDIA = "https://api.parse.com/1/classes/media_content?where={\"feed_id\":\"%s\"}";
    public static final String URL_COMMENTS = "https://api.parse.com/1/classes/comments?where={\"feed_id\":\"%s\"}";
    public static final String URL_USERS_EMAIL_PASSWORD = "https://api.parse.com/1/classes/users?where={\"email\":\"%s\"," +
            "\"password\":\"%s\"}";
    public static final String URL_USERS_EMAIL = "https://api.parse.com/1/classes/users?where={\"email\":\"%s\"}";
    public static final String FEED_TABLE = "data";

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_TYPE_VALUE = "application/json";
    public static final String APP_ID = "X-Parse-Application-Id";
    public static final String APP_ID_VALUE = "9k71L2YA8j3bzXr7gI0HLQHPlwchaAAhqWezaSrT";
    public static final String API_KEY = "X-Parse-REST-API-Key";
    public static final String API_KEY_VALUE = "3yRLxMS7WYFWWI0RG81x7sZ8k7cWjGSDw7OCLe01";
    public static final String HTTP_METHOD_GET = "GET";
    public static final int READ_TIMEOUT = 10000;
    public static final int CONNECT_TIMEOUT = 15000;

    public static final int ERROR_UPDATE_RATING = 1;
    public static final int ERROR_ADD_USER = 2;
    public static final int ERROR_UPDATE_ACCESS_TOKEN = 3;
    public static final int ERROR_VALID_TOKEN = 4;

    public static InputStream getDataStream(String baseUrl, @Nullable String id) throws IOException {
        URL url;
        if (id == null) {
            url = new URL(baseUrl);
        } else {
            String strUrl = String.format(baseUrl, id);
            url = new URL(strUrl);
        }
        return getConnection(url).getInputStream();
    }

    public static InputStream getUserStream(String email, @Nullable String password) throws IOException {
        URL url;
        if (password != null) {
            url = new URL(String.format(URL_USERS_EMAIL_PASSWORD, email, password));
        } else {
            url = new URL(String.format(URL_USERS_EMAIL, email));
        }
        return getConnection(url).getInputStream();

    }

    public static void addUser(final int callbacksToken, User user, final AddUserCallbacks addUserCallbacks) {
        final WeakReference<AddUserCallbacks> callbacks = new WeakReference<>(addUserCallbacks);
        final ParseObject newUser = new ParseObject(User.UserEntry.TABLE_USERS);
        newUser.put(User.UserEntry.COLUMN_USER_NAME, user.getUserName());
        newUser.put(User.UserEntry.COLUMN_EMAIL, user.getEmail());
        newUser.put(User.UserEntry.COLUMN_PASSWORD, user.getPassword());
        newUser.put(User.UserEntry.COLUMN_ACCESS_TOKEN, user.getAccessToken());
        newUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    callbacks.get().successfully(callbacksToken, newUser.getObjectId());
                } else {
                    callbacks.get().error(ERROR_ADD_USER);
                }
            }
        });
    }

    public static void updateRating(final int token, String feedId, final int rating, final ConnectionCallbacks connectionCallbacks) {
        final WeakReference<ConnectionCallbacks> callbacks = new WeakReference<>(connectionCallbacks);
        ParseQuery<ParseObject> query = ParseQuery.getQuery(FEED_TABLE);
        query.getInBackground(feedId, new GetCallback<ParseObject>() {
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    parseObject.put(Feed.FeedEntry.COLUMN_FEED_RATING, rating);
                    parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            if (e == null) {
                                callbacks.get().successfully(token);
                            } else {
                                callbacks.get().error(ERROR_UPDATE_RATING);
                            }
                        }
                    });
                }
            }
        });
    }

    public static void updateAccessToken(
            final int callbacksToken, final String accessToken, String userId, ConnectionCallbacks connectionCallbacks) {

        final WeakReference<ConnectionCallbacks> callbacks = new WeakReference<>(connectionCallbacks);
        ParseQuery<ParseObject> query = ParseQuery.getQuery(User.UserEntry.TABLE_USERS);
        query.getInBackground(userId, new GetCallback<ParseObject>() {
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    parseObject.put(User.UserEntry.COLUMN_ACCESS_TOKEN, accessToken);
                    parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            if (e == null) {
                                callbacks.get().successfully(callbacksToken);
                            } else {
                                callbacks.get().error(ERROR_UPDATE_ACCESS_TOKEN);
                            }
                        }
                    });
                }
            }
        });
    }

    public static void checkValidAccessToken(
            final int callbacksToken, final String accessToken, String userId, ConnectionCallbacks connectionCallbacks) {

        final WeakReference<ConnectionCallbacks> callbacks = new WeakReference<>(connectionCallbacks);
        ParseQuery<ParseObject> query = ParseQuery.getQuery(User.UserEntry.TABLE_USERS);
        query.getInBackground(userId, new GetCallback<ParseObject>() {
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    if (accessToken.equals(parseObject.getString(User.UserEntry.COLUMN_ACCESS_TOKEN))) {
                        callbacks.get().successfully(callbacksToken);
                    } else {
                        callbacks.get().error(ERROR_VALID_TOKEN);
                    }
                }
            }
        });
    }

    private static HttpURLConnection getConnection(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(READ_TIMEOUT);
        conn.setConnectTimeout(CONNECT_TIMEOUT);
        conn.setDoInput(true);

        conn.setRequestProperty(CONTENT_TYPE, CONTENT_TYPE_VALUE);
        conn.setRequestProperty(APP_ID, APP_ID_VALUE);
        conn.setRequestProperty(API_KEY, API_KEY_VALUE);
        conn.setRequestMethod(HTTP_METHOD_GET);
        conn.connect();
        return conn;
    }

    public interface ConnectionCallbacks {
        void successfully(int callbacksToken);

        void error(int errorToken);
    }

    public interface AddUserCallbacks {
        void successfully(int callbacksToken, String objId);

        void error(int errorToken);
    }
}
