package com.onregs.recyclerviewexample.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.ui.adapter.holder.FeedViewHolder;

/**
 * Created by vadim on 27.11.2015.
 */
public class FeedListCursorAdapter extends CursorRecyclerViewAdapter<FeedViewHolder> implements View.OnClickListener {

    private OnItemClickListener mOnItemClickListener;

    public FeedListCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor);
    }

    @Override
    public FeedViewHolder newViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.feed_item, viewGroup, false);
        view.setOnClickListener(this);
        return new FeedViewHolder(view, getContext(), false);
    }

    @Override
    public void bindViewHolder(FeedViewHolder viewHolder, Cursor cursor) {
        viewHolder.bindView(cursor);
    }

    @Override
    public void onViewRecycled(FeedViewHolder holder) {
        holder.clearImage();
        super.onViewRecycled(holder);
    }

    public void setOnTemClickListener(OnItemClickListener onTemClickListener) {
        this.mOnItemClickListener = onTemClickListener;
    }

    @Override
    public void onClick(View view) {
        int position = ((RecyclerView.LayoutParams) view.getLayoutParams()).getViewLayoutPosition();
        Cursor cursor = getCursor();
        cursor.moveToPosition(position);
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(cursor);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Cursor cursor);
    }
}
