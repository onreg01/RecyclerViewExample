package com.onregs.recyclerviewexample.ui.activity;

import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.Comment;
import com.onregs.recyclerviewexample.data.FavoriteFeed;
import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.db.content_provider.DataContentProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.FeedContentProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.async_query_handler.CustomAsyncQueryHandler;
import com.onregs.recyclerviewexample.data.loader.ContentLoader;
import com.onregs.recyclerviewexample.data.loader.LoaderAnswer;
import com.onregs.recyclerviewexample.ui.adapter.DetailViewCursorAdapter;
import com.onregs.recyclerviewexample.utils.DateUtils;
import com.onregs.recyclerviewexample.utils.ImageSwitcher;
import com.onregs.recyclerviewexample.utils.InternetUtils;

public class DetailViewActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener, CustomAsyncQueryHandler.AsyncListener, CustomAsyncQueryHandler.AsyncQueryListener {

    public static final String SHORT_TYPE = "DESC";
    public static final String SELECTION_FEED = Feed.FeedEntry.TABLE_FEEDS + "." + Feed.FeedEntry._ID + "=?";
    public static final String SELECTION_CURRENT_FEED = Comment.CommentsContentEntry.COLUMN_FEED_ID + "=?";
    public static final String ORDER_BY = Comment.CommentsContentEntry.COLUMN_COMMENT_CREATED_AT + " " + SHORT_TYPE;

    public static final int DETAIL_TOKEN = 0;
    public static final int COMMENTS_TOKEN = 1;

    public boolean mIsFavorite;
    private String mCurrentFeedId;
    private Cursor mHeader;

    private ImageSwitcher mImageSwitcher;
    private DetailViewCursorAdapter mDetailViewCursorAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mDetailRecyclerView;

    private ContentLoadCallbacks mContentLoadCallbacks = new ContentLoadCallbacks();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_view);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mCurrentFeedId = getIntent().getStringExtra(Feed.FeedEntry._ID);

        initDetailViewList();

        new CustomAsyncQueryHandler(getContentResolver()).setQueryListener(this).startQuery(DETAIL_TOKEN, null,
                FeedContentProvider.CONTENT_URI_FEEDS, FeedContentProvider.FEED_PROJECTION,
                SELECTION_FEED, new String[]{mCurrentFeedId}, null);
    }

    private void init() {
        if (mHeader != null) {
            mHeader.moveToFirst();
            setFavorite();
            setToolbar(mHeader);
            if (DateUtils.checkFeedTimeStamp(mHeader.getLong(mHeader.getColumnIndex(Feed.FeedEntry.COLUMN_FEED_CONTENT_TIME_STAMP)))) {
                startContentLoader();
            }
        }
        new CustomAsyncQueryHandler(getContentResolver()).setQueryListener(this).startQuery(COMMENTS_TOKEN, null,
                DataContentProvider.CONTENT_URI_COMMENTS, null, SELECTION_CURRENT_FEED, new String[]{mCurrentFeedId}, ORDER_BY);
    }

    private void setFavorite() {
        String isFavorite = mHeader.getString(mHeader.getColumnIndex(FavoriteFeed.IS_FAVORITE));
        mIsFavorite = !(FavoriteFeed.NOT_FAVORITE.equals(isFavorite) || isFavorite == null);
    }

    private void initAdapter(Cursor cursor) {
        mDetailViewCursorAdapter = new DetailViewCursorAdapter(this, cursor, mCurrentFeedId);
        mDetailViewCursorAdapter.setHeaderFooter(mHeader, null, mDetailViewCursorAdapter);
        mDetailRecyclerView.setAdapter(mDetailViewCursorAdapter);
    }

    private void initDetailViewList() {
        mDetailRecyclerView = (RecyclerView) findViewById(R.id.comments_list);
        mDetailRecyclerView.setFocusable(false);
        mDetailRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mDetailRecyclerView.setLayoutManager(layoutManager);
    }

    private void startContentLoader() {
        if (InternetUtils.checkNetwork(this)) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
            Bundle bundle = new Bundle();
            bundle.putString(Feed.FeedEntry._ID, mCurrentFeedId);
            getLoaderManager().restartLoader(0, bundle, mContentLoadCallbacks);
        } else {
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(this, getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void setToolbar(Cursor cursor) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(cursor.getString(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_FEED_TITTLE)));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private class ContentLoadCallbacks implements LoaderManager.LoaderCallbacks<LoaderAnswer> {
        @Override
        public Loader<LoaderAnswer> onCreateLoader(int id, Bundle args) {
            return new ContentLoader(DetailViewActivity.this, args.getString(Feed.FeedEntry._ID));
        }

        @Override
        public void onLoadFinished(Loader<LoaderAnswer> loader, LoaderAnswer loaderAnswer) {
            mSwipeRefreshLayout.setRefreshing(false);
            if (loaderAnswer.getResult()) {
                getContentResolver().notifyChange(DataContentProvider.CONTENT_URI_MEDIA, null);
                getContentResolver().notifyChange(DataContentProvider.CONTENT_URI_COMMENTS, null);
            }
            getLoaderManager().destroyLoader(0);
            Toast.makeText(DetailViewActivity.this, loaderAnswer.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onLoaderReset(Loader<LoaderAnswer> loader) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDetailViewCursorAdapter.closeCursor();
    }

    @Override
    public void onRefresh() {
        startContentLoader();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_activity_menu, menu);
        MenuItem menuItem = menu.getItem(0);
        mImageSwitcher = new ImageSwitcher(menuItem,
                R.drawable.ic_action_toggle_star_outline, R.drawable.ic_action_toggle_star, mIsFavorite);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorite:
                if (mImageSwitcher.onAction()) {
                    new CustomAsyncQueryHandler(getContentResolver()).setListener(this).addFeedToFavorite(mCurrentFeedId);
                } else {
                    new CustomAsyncQueryHandler(getContentResolver()).setListener(this).deleteFeedFromFavorite(mCurrentFeedId);
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onComplete() {
        getContentResolver().notifyChange(FeedContentProvider.CONTENT_URI_FEEDS, null);
    }

    @Override
    public void onQueryComplete(int token, Object cookie, Cursor cursor) {
        if (token == DETAIL_TOKEN) {
            mHeader = cursor;
            init();
        } else {
            initAdapter(cursor);
        }
    }

}
