package com.onregs.recyclerviewexample.ui.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.app.DefaultApplication;
import com.onregs.recyclerviewexample.service.MoveService;

import java.util.Locale;

public class MoveCheckActivity extends AppCompatActivity {

    private MoveService service;
    private boolean mBound = false;
    private TextView coordinates;
    private TextView distance;
    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_check);
        setToolbar();
        setReceiver();
        coordinates = (TextView) findViewById(R.id.coordinates);
        distance = (TextView) findViewById(R.id.distance);

        findViewById(R.id.check_distance).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                distance.setText(String.valueOf(service.getDistance()));
            }
        });

        findViewById(R.id.reset_distance).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service.resetDistance();
                distance.setText("");
            }
        });
        startService(new Intent(this, MoveService.class));
    }



    private void setReceiver() {
        receiver = new LocationReceiver(callbacks);
        IntentFilter filter = new IntentFilter(LocationReceiver.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoveCheckActivity.super.onBackPressed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MoveService.class);
        bindService(intent, serviceConnection, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(serviceConnection);
            mBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        stopService(new Intent(this, MoveService.class));
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            MoveService.MoveServiceBinder binder = (MoveService.MoveServiceBinder) iBinder;
            service = binder.getMoveService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    LocationReceiver.Callbacks callbacks = new LocationReceiver.Callbacks() {

        @Override
        public void updateLocation(double latitude, double longitude) {
            coordinates.setText(String.format(Locale.getDefault(), "%f  %f", latitude, longitude));
        }
    };

    public static class LocationReceiver extends BroadcastReceiver {
        public final static String BROADCAST_ACTION = "location.listener";
        public final static String LATITUDE = "latitude";
        public final static String LONGITUDE = "longitude";

        private Callbacks callbacks;

        public LocationReceiver(Callbacks callbacks) {
            this.callbacks = callbacks;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            callbacks.updateLocation(intent.getDoubleExtra(LATITUDE, 0), intent.getDoubleExtra(LONGITUDE, 0));
        }

        interface Callbacks {
            void updateLocation(double latitude, double longitude);
        }
    }

}
