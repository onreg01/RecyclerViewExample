package com.onregs.recyclerviewexample.ui.activity;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.User;
import com.onregs.recyclerviewexample.data.loader.UserChecker;

public class SplashScreenActivity extends AppCompatActivity {
    public static final int CHECK_USER_LOADER = 0;

    public static final int DELAY = 1000; //1s
    public static final int NO_AUTHORIZE_USERS = 0;
    public static final int AUTHORIZE_USERS = 1;
    public static final int PROCESSING_AUTHORIZE = -1;

    private int checkUserResult = PROCESSING_AUTHORIZE;
    private boolean isDelayEnd;
    private User mUser;

    private CheckUserCallbacks mCheckUserCallbacks = new CheckUserCallbacks();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spash_screen);

        getLoaderManager().initLoader(CHECK_USER_LOADER, null, mCheckUserCallbacks);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkUserResult != PROCESSING_AUTHORIZE) {
                    if (checkUserResult == AUTHORIZE_USERS) {
                        startFeedActivity();
                    } else {
                        startLoginActivity();
                    }
                }
                isDelayEnd = true;
            }
        }, DELAY);
    }

    private void startLoginActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    private void startFeedActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, FeedActivity.class);
        intent.putExtra(User.UserEntry.COLUMN_USER_NAME, mUser.getUserName());
        intent.putExtra(User.UserEntry.COLUMN_EMAIL, mUser.getEmail());
        intent.putExtra(User.UserEntry.COLUMN_ACCOUNT_NAME, mUser.getAccountName());
        intent.putExtra(User.UserEntry._ID, mUser.getId());
        startActivity(intent);
    }


    private class CheckUserCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
            return new UserChecker(SplashScreenActivity.this);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            if (cursor.moveToFirst()) {
                setUser(cursor);
                checkUserResult = AUTHORIZE_USERS;
                if (isDelayEnd) {
                    startFeedActivity();
                }
            } else {
                checkUserResult = NO_AUTHORIZE_USERS;
                if (isDelayEnd) {
                    startLoginActivity();
                }
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    }

    private void setUser(Cursor cursor) {
        mUser = new User();
        mUser.setId(cursor.getString(cursor.getColumnIndex(User.UserEntry._ID)));
        mUser.setUserName(cursor.getString(cursor.getColumnIndex(User.UserEntry.COLUMN_USER_NAME)));
        mUser.setEmail(cursor.getString(cursor.getColumnIndex(User.UserEntry.COLUMN_EMAIL)));
        mUser.setAccountName(cursor.getString(cursor.getColumnIndex(User.UserEntry.COLUMN_ACCOUNT_NAME)));
    }
}
