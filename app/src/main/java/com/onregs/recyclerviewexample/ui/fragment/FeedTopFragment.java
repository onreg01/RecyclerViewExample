package com.onregs.recyclerviewexample.ui.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;

import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.db.content_provider.FeedContentProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.async_query_handler.CustomAsyncQueryHandler;
import com.onregs.recyclerviewexample.utils.DateUtils;

/**
 * Created by vadim on 22.11.2015.
 */
public class FeedTopFragment extends FeedFragment implements CustomAsyncQueryHandler.AsyncQueryListener {

    public static final String SHORT_TYPE = "DESC";
    public static final String ORDER_BY = Feed.FeedEntry.COLUMN_FEED_RATING + " " + SHORT_TYPE;
    public static final String SELECTION = Feed.FeedEntry.TABLE_FEEDS + "." + Feed.FeedEntry.COLUMN_FEED_CREATED_AT + " >= ?";
    public static final String DEFAULT_SELECTION = "0";

    public static final int INIT_TOKEN = 0;

    private boolean isNewFragment;

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);

        new CustomAsyncQueryHandler(getActivity().getContentResolver()).setQueryListener(this).startQuery(
                INIT_TOKEN, null, FeedContentProvider.CONTENT_URI_FEEDS, FeedContentProvider.FEED_PROJECTION, SELECTION,
                getSelectionArgs(), ORDER_BY);

        isNewFragment = true;
    }

    public String[] getSelectionArgs() {
        String[] selectionArgs = new String[]{DEFAULT_SELECTION};

        String arg = DateUtils.getPeriod(getActivity());
        if (!arg.equals(DateUtils.ALL_TIME)) {
            selectionArgs[0] = arg;
        }
        return selectionArgs;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isNewFragment) {
            new CustomAsyncQueryHandler(getActivity().getContentResolver()).setQueryListener(this).startQuery(INIT_TOKEN, null,
                    FeedContentProvider.CONTENT_URI_FEEDS, FeedContentProvider.FEED_PROJECTION, SELECTION, getSelectionArgs(),
                    ORDER_BY);
        }
        isNewFragment = false;
    }

    @Override
    public void onQueryComplete(int token, Object cookie, Cursor cursor) {
        if (token == INIT_TOKEN) {
            setAdapter(cursor);
        } else {
            changeCursor(cursor);
        }
    }
}
