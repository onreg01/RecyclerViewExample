package com.onregs.recyclerviewexample.ui.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;

import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.db.content_provider.FeedContentProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.async_query_handler.CustomAsyncQueryHandler;

/**
 * Created by vadim on 22.11.2015.
 */
public class FeedNewFragment extends FeedFragment implements CustomAsyncQueryHandler.AsyncQueryListener {

    private static final String SHORT_TYPE = "DESC";
    public static final String ORDER_BY = Feed.FeedEntry.TABLE_FEEDS + "." + Feed.FeedEntry.COLUMN_FEED_CREATED_AT + " " + SHORT_TYPE;
    public static final int NEW_FEED_TOKEN = 0;

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);

        new CustomAsyncQueryHandler(getActivity().getContentResolver()).setQueryListener(this).startQuery(NEW_FEED_TOKEN,
                null, FeedContentProvider.CONTENT_URI_FEEDS, FeedContentProvider.FEED_PROJECTION,
                null, null, ORDER_BY);
    }

    @Override
    public void onQueryComplete(int token, Object cookie, Cursor cursor) {
        setAdapter(cursor);
    }
}