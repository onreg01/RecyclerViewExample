package com.onregs.recyclerviewexample.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.db.content_provider.FeedContentProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.async_query_handler.CustomAsyncQueryHandler;
import com.onregs.recyclerviewexample.ui.adapter.holder.FeedViewHolder;

/**
 * Created by vadim on 18.12.2015.
 */
public class FavoriteFeedListCursorAdapter extends FeedListCursorAdapter {

    public FavoriteFeedListCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor);
    }

    @Override
    public FeedViewHolder newViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.feed_item, viewGroup, false);
        view.setOnClickListener(this);
        return new FavoriteFeedViewHolder(view, getContext());
    }

    public class FavoriteFeedViewHolder extends FeedViewHolder {
        public FavoriteFeedViewHolder(View itemView, Context context) {
            super(itemView, context, true);
        }

        @Override
        public void onClick(View v) {
            new CustomAsyncQueryHandler(
                    getContext().getContentResolver()).setListener(this).deleteFeedFromFavorite(getFeedId());
            getContext().getContentResolver().notifyChange(FeedContentProvider.CONTENT_URI_FEEDS, null);
        }
    }
}
