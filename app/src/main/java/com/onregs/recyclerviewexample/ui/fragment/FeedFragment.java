package com.onregs.recyclerviewexample.ui.fragment;

import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.listener.ListItemClickListener;
import com.onregs.recyclerviewexample.ui.adapter.FeedListCursorAdapter;
import com.onregs.recyclerviewexample.ui.adapter.decorator.ItemDivider;

/**
 * Created by vadim on 22.11.2015.
 */
public class FeedFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private FeedListCursorAdapter mFeedListCursorAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feed_fragment, container, false);
        initRecyclerView(view);
        return view;
    }

    private void initRecyclerView(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rvFeedList);

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
    }

    protected void setAdapter(Cursor cursor) {
        mFeedListCursorAdapter = new FeedListCursorAdapter(getActivity(), cursor);
        mFeedListCursorAdapter.setOnTemClickListener(new ListItemClickListener(getActivity()));
        mRecyclerView.setAdapter(mFeedListCursorAdapter);
        mRecyclerView.addItemDecoration(new ItemDivider(getActivity()));
    }

    protected void changeCursor(Cursor cursor) {
        mFeedListCursorAdapter.changeCursor(cursor);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFeedListCursorAdapter.closeCursor();
    }
}
