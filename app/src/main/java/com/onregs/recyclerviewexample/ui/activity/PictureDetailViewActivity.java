package com.onregs.recyclerviewexample.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.MediaContent;
import com.onregs.recyclerviewexample.data.loader.ImageLoader;

public class PictureDetailViewActivity extends AppCompatActivity implements ImageLoader.Callbacks {

    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_detail_view);

        setToolbar();
        setImage();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void setImage() {
        mImageView = (ImageView) findViewById(R.id.picture);
        ImageLoader.getInstance(this).load(getIntent().getStringExtra(MediaContent.MediaContentEntry.COLUMN_FIELD_LINK), true, this);
    }

    @Override
    public void successfully(Bitmap bitmap) {
        mImageView.setImageBitmap(bitmap);
    }

    @Override
    public void failLoad(Bitmap bitmap) {
        mImageView.setImageBitmap(bitmap);
    }
}
