package com.onregs.recyclerviewexample.ui.activity;

import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.AccountPicker;
import com.onregs.recyclerviewexample.Const;
import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.connection.ParseComConnection;
import com.onregs.recyclerviewexample.connection.TokenHandler;
import com.onregs.recyclerviewexample.data.User;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;
import com.onregs.recyclerviewexample.data.loader.UserLoader;
import com.onregs.recyclerviewexample.utils.EditTextChecker;
import com.onregs.recyclerviewexample.utils.InternetUtils;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ParseComConnection.ConnectionCallbacks {

    private EditText mEditTextMail;
    private EditText mEditTextPassword;
    private TextInputLayout[] mLabels = new TextInputLayout[2];

    private Button mButtonLogin;
    private Button mButtonRegister;

    private String mEmail;
    private String mPassword;

    private static final String USER_EMAIL = "user_email";
    private static final String USER_PASSWORD = "user_password";
    public static final int UPDATE_ACCESS_TOKEN = 1;
    public static final int SELECTION_ACCOUNT = 2;


    public static final int LOGIN_LOADER = 0;
    private LoginCallbacks mLoginCallbacks = new LoginCallbacks();
    private AccessTokenCallbacks mAccessTokenCallbacks = new AccessTokenCallbacks();

    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initEditTexts();
        initLabels();
        initButtonLogin();
        initButtonRegister();
    }

    private void initLabels() {
        mLabels[0] = (TextInputLayout) findViewById(R.id.email_label);
        mLabels[1] = (TextInputLayout) findViewById(R.id.password_label);
    }

    private void initEditTexts() {
        mEditTextMail = (EditText) findViewById(R.id.email_edit_text);
        mEditTextPassword = (EditText) findViewById(R.id.password_edit_text);
    }

    private void initButtonLogin() {
        mButtonLogin = (Button) findViewById(R.id.button_login);
        mButtonLogin.setOnClickListener(this);
    }

    private void initButtonRegister() {
        mButtonRegister = (Button) findViewById(R.id.button_register);
        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {

        for (TextInputLayout label : mLabels) {
            label.setErrorEnabled(false);
        }

        if (checkDataFields()) {
            Bundle bundle = new Bundle();
            bundle.putString(USER_EMAIL, mEmail);
            bundle.putString(USER_PASSWORD, mPassword);

            if (InternetUtils.checkNetwork(this)) {
                disableButtons();
                getLoaderManager().restartLoader(LOGIN_LOADER, bundle, mLoginCallbacks);
            } else {
                Toast.makeText(this, getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void disableButtons() {
        mButtonLogin.setEnabled(false);
        mButtonRegister.setEnabled(false);
    }

    private void enableButtons() {
        mButtonLogin.setEnabled(true);
        mButtonRegister.setEnabled(true);
    }

    private boolean checkDataFields() {
        if (!checkEditTextEmail()) return false;
        return checkEditTextPasswords();
    }

    private boolean checkEditTextPasswords() {
        Editable password = mEditTextPassword.getText();
        if (!EditTextChecker.checkIfEmpty(this, mLabels[1], password)) {
            return false;
        }
        mPassword = password.toString();
        return true;
    }

    private boolean checkEditTextEmail() {
        Editable email = mEditTextMail.getText();
        if (!EditTextChecker.checkEmailField(this, mLabels[0], email)) {
            return false;
        }
        mEmail = email.toString();
        return true;
    }

    private void startFeedActivity() {
        Intent intent = new Intent(this, FeedActivity.class);
        intent.putExtra(User.UserEntry.COLUMN_USER_NAME, mUser.getUserName());
        intent.putExtra(User.UserEntry.COLUMN_EMAIL, mUser.getEmail());
        intent.putExtra(User.UserEntry.COLUMN_ACCOUNT_NAME, mUser.getAccountName());
        intent.putExtra(User.UserEntry._ID, mUser.getId());

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void successfully(int callbacksToken) {
        startFeedActivity();
    }

    @Override
    public void error(int errorToken) {
        if (errorToken == ParseComConnection.ERROR_UPDATE_ACCESS_TOKEN) {
            Toast.makeText(this, getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == SELECTION_ACCOUNT && resultCode == RESULT_OK) {
            mUser.setAccountName(data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME));
            try {
                new TokenHandler(LoginActivity.this, mUser.getAccountName(), mAccessTokenCallbacks).getAccessToken();
            } catch (OperationCanceledException e) {
                Log.e(Const.ERROR, e.getMessage());
                Toast.makeText(this, getString(R.string.account_error), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class AccessTokenCallbacks implements TokenHandler.TokenHandlerCallbacks {

        @Override
        public void run(AccountManagerFuture<Bundle> future) {
            try {
                Bundle bundle = future.getResult();
                final String accessToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        DataSqliteOpenHelper.getInstance(LoginActivity.this).updateUserData(accessToken, mUser.getAccountName());
                    }
                }).start();

                ParseComConnection.updateAccessToken(UPDATE_ACCESS_TOKEN, accessToken, mUser.getId(), LoginActivity.this);

            } catch (android.accounts.OperationCanceledException | IOException | AuthenticatorException e) {
                Log.e(Const.ERROR, e.getMessage());
            }
        }

        @Override
        public boolean handleMessage(Message msg) {
            Toast.makeText(LoginActivity.this, getString(R.string.get_access_token_error), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private class LoginCallbacks implements LoaderManager.LoaderCallbacks<UserLoader.UserLoaderAnswer> {

        @Override
        public Loader<UserLoader.UserLoaderAnswer> onCreateLoader(int id, Bundle bundle) {
            return new UserLoader(LoginActivity.this, bundle.getString(USER_EMAIL), bundle.getString(USER_PASSWORD),
                    UserLoader.ACTION_LOGIN);
        }

        @Override
        public void onLoadFinished(Loader<UserLoader.UserLoaderAnswer> loader, UserLoader.UserLoaderAnswer answer) {
            if (answer.getResult()) {
                mUser = answer.getUser();
                Intent intent = AccountPicker.zza(null, null, new String[]{TokenHandler.ACCOUNT_TYPE}, false, null, null, null, null, false, 1, 0);
                startActivityForResult(intent, SELECTION_ACCOUNT);
            } else {
                enableButtons();
                Toast.makeText(LoginActivity.this, answer.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onLoaderReset(Loader<UserLoader.UserLoaderAnswer> loader) {

        }
    }
}
