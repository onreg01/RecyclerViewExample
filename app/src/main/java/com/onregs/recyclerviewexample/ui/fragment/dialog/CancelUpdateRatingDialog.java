package com.onregs.recyclerviewexample.ui.fragment.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.onregs.recyclerviewexample.R;

/**
 * Created by vadim on 25.12.2015.
 */
public class CancelUpdateRatingDialog extends DialogFragment implements View.OnClickListener {

    private CancelUpdateRatingDialogCallbacks mCallbacks;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.doalog_cacel_change_rating, null);
        view.findViewById(R.id.button_positive).setOnClickListener(this);
        view.findViewById(R.id.button_negative).setOnClickListener(this);
        builder.setView(view);

        AlertDialog dialogFragment = builder.create();
        dialogFragment.setCanceledOnTouchOutside(false);
        return dialogFragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_positive:
                mCallbacks.onCancelUpdateRating();
                dismiss();
                break;
            case R.id.button_negative:
                dismiss();
                break;
        }
    }

    public void setCallbacks(CancelUpdateRatingDialogCallbacks callbacks) {
        this.mCallbacks = callbacks;
    }

    public interface CancelUpdateRatingDialogCallbacks {
        void onCancelUpdateRating();
    }
}
