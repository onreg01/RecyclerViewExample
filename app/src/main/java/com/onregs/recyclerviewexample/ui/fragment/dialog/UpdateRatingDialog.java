package com.onregs.recyclerviewexample.ui.fragment.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.RadioGroup;

import com.onregs.recyclerviewexample.R;

/**
 * Created by vadim on 24.12.2015.
 */
public class UpdateRatingDialog extends DialogFragment implements View.OnClickListener {

    private UpdateRatingDialogCallbacks mCallbacks;
    private RadioGroup mRadioGroup;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_update_rating, null);
        mRadioGroup = (RadioGroup) view.findViewById(R.id.rating);
        view.findViewById(R.id.button_positive).setOnClickListener(this);
        view.findViewById(R.id.button_negative).setOnClickListener(this);
        builder.setView(view);

        AlertDialog dialogFragment = builder.create();
        dialogFragment.setCanceledOnTouchOutside(false);
        return dialogFragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_positive:
                positiveActions();
                dismiss();
                break;
            case R.id.button_negative:
                dismiss();
                break;
        }
    }

    private void positiveActions() {
        switch (mRadioGroup.getCheckedRadioButtonId()) {
            case R.id.rating_up:
                mCallbacks.increaseRating();
                break;
            case R.id.rating_down:
                mCallbacks.decreaseRating();
                break;
        }
    }

    public void setCallbacks(UpdateRatingDialogCallbacks callbacks) {
        this.mCallbacks = callbacks;
    }

    public interface UpdateRatingDialogCallbacks {
        void increaseRating();

        void decreaseRating();
    }
}
