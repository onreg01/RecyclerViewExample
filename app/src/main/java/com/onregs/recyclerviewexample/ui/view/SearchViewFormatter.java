package com.onregs.recyclerviewexample.ui.view;

import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.onregs.recyclerviewexample.Const;
import com.onregs.recyclerviewexample.R;

import java.lang.reflect.Field;

public class SearchViewFormatter {

    public static final String CURSOR_DRAWABLE = "mCursorDrawableRes";

    private int mSearchTextColorResource = 0;
    private int mSearchHintColorResource = 0;
    private int mCursor = 0;

    public SearchViewFormatter setSearchTextColorResource(int searchTextColorResource) {
        mSearchTextColorResource = searchTextColorResource;
        return this;
    }

    public SearchViewFormatter setSearchHintColorResource(int searchHintColorResource) {
        mSearchHintColorResource = searchHintColorResource;
        return this;
    }

    public SearchViewFormatter setCursorColorResource(int cursor) {
        mCursor = cursor;
        return this;
    }

    public void format(SearchView searchView) {
        if (searchView == null) {
            return;
        }
        View searchText = searchView.findViewById(R.id.search_src_text);
        if (mSearchTextColorResource != 0) {
            ((TextView) searchText).setTextColor(mSearchTextColorResource);

        }
        if (mSearchHintColorResource != 0) {
            ((TextView) searchText).setHintTextColor(mSearchHintColorResource);
        }

        if (mCursor != 0) {
            try {
                Field f = TextView.class.getDeclaredField(CURSOR_DRAWABLE);
                f.setAccessible(true);
                f.set(searchText, mCursor);
            } catch (Exception e) {
                Log.d(Const.ERROR, e.getMessage());
            }
        }


    }

}