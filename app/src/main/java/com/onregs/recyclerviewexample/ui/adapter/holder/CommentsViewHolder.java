package com.onregs.recyclerviewexample.ui.adapter.holder;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.Comment;
import com.onregs.recyclerviewexample.utils.DateUtils;

/**
 * Created by vadim on 13.12.2015.
 */
public class CommentsViewHolder extends RecyclerView.ViewHolder {
    private TextView mUser;
    private TextView mComment;
    private TextView mCommentTime;

    public static final String FORMAT_USER = "%s %s";

    public CommentsViewHolder(View itemView) {
        super(itemView);
        mUser = (TextView) itemView.findViewById(R.id.user);
        mComment = (TextView) itemView.findViewById(R.id.comment);
        mCommentTime = (TextView) itemView.findViewById(R.id.comment_time);
    }

    public void bindView(Cursor cursor) {

        String userName = cursor.getString(cursor.getColumnIndex(Comment.CommentsContentEntry.COLUMN_NAME));
        String userLast = cursor.getString(cursor.getColumnIndex(Comment.CommentsContentEntry.COLUMN_LAST_NAME));
        mUser.setText(String.format(FORMAT_USER, userName, userLast));

        mComment.setText(cursor.getString(cursor.getColumnIndex(Comment.CommentsContentEntry.COLUMN_COMMENT)));
        mCommentTime.setText(DateUtils.getStrData(
                cursor.getLong(cursor.getColumnIndex(Comment.CommentsContentEntry.COLUMN_COMMENT_CREATED_AT))));
    }


}
