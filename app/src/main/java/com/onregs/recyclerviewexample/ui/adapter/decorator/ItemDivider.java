package com.onregs.recyclerviewexample.ui.adapter.decorator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.onregs.recyclerviewexample.R;

/**
 * Created by vadim on 25.12.2015.
 */
public class ItemDivider extends RecyclerView.ItemDecoration {

    private Drawable mDivider;
    private Context mContext;

    public ItemDivider(Context context) {
        this.mDivider = context.getResources().getDrawable(R.drawable.item_divider);
        this.mContext = context;
    }

    @Override
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        int left = (int) mContext.getResources().getDimension(R.dimen.left_margin);
        int right = recyclerView.getWidth() - recyclerView.getPaddingRight();

        int childCount = recyclerView.getChildCount();
        for (int i = 0; i < childCount - 1; i++) {
            View child = recyclerView.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(canvas);
        }
    }
}
