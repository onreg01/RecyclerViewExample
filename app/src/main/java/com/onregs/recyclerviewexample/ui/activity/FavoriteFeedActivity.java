package com.onregs.recyclerviewexample.ui.activity;

import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.FavoriteFeed;
import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;
import com.onregs.recyclerviewexample.data.db.content_provider.FeedContentProvider;
import com.onregs.recyclerviewexample.data.loader.FavoriteFeedLoader;
import com.onregs.recyclerviewexample.listener.ListItemClickListener;
import com.onregs.recyclerviewexample.ui.adapter.FavoriteFeedListCursorAdapter;
import com.onregs.recyclerviewexample.ui.adapter.decorator.ItemDivider;

public class FavoriteFeedActivity extends AppCompatActivity {

    public static final String TABLE =
            Feed.FeedEntry.TABLE_FEEDS + " inner join " +
                    FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS + " on " +
                    Feed.FeedEntry.TABLE_FEEDS + "." +
                    Feed.FeedEntry._ID + " = " +
                    FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS + "." +
                    FavoriteFeed.FavoriteFeedEntry.COLUMN_FEED_ID;

    public static final String[] PROJECTION = {Feed.FeedEntry.TABLE_FEEDS + "." + Feed.FeedEntry._ID,
            Feed.FeedEntry.COLUMN_FEED_TITTLE, Feed.FeedEntry.COLUMN_FEED_RATING, Feed.FeedEntry.COLUMN_FEED_LINK,
            Feed.FeedEntry.COLUMN_FEED_DESCRIPTION, Feed.FeedEntry.COLUMN_FEED_COMMENTS_COUNT,
            Feed.FeedEntry.COLUMN_FEED_TYPE, Feed.FeedEntry.COLUMN_IMAGE_URL};

    public static final String SELECTION = FavoriteFeed.FavoriteFeedEntry.COLUMN_USER_ID + " = " +
            DataSqliteOpenHelper.CustomQueryBuilder.SELECT_CURRENT_USER_ID;

    private static final String SHORT_TYPE = "DESC";
    public static final String ORDER_BY = FavoriteFeed.FavoriteFeedEntry.TABLE_FAVORITE_FEEDS + "." +
            FavoriteFeed.FavoriteFeedEntry.COLUMN_CREATED_AT + " " + SHORT_TYPE;

    private RecyclerView mRecyclerView;

    private static final String BUNDLE_TABLE = "table";
    private static final String BUNDLE_PROJECTION = "projection";
    private static final String BUNDLE_SELECTION = "seletion";
    private static final String BUNDLE_ORDER_BY = "order_by";

    private FavoriteFeedsLoaderCallbacks mFavoriteFeedsLoaderCallbacks = new FavoriteFeedsLoaderCallbacks();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_feed);
        setToolbar();
        initList();

        startFavoriteFeedLoader();
    }

    private void startFavoriteFeedLoader() {
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_TABLE, TABLE);
        bundle.putStringArray(BUNDLE_PROJECTION, PROJECTION);
        bundle.putString(BUNDLE_SELECTION, SELECTION);

        bundle.putString(BUNDLE_ORDER_BY, ORDER_BY);
        getLoaderManager().restartLoader(0, bundle, mFavoriteFeedsLoaderCallbacks);
    }

    private void initList() {
        mRecyclerView = (RecyclerView) findViewById(R.id.favorite_list);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void setAdapter(Cursor cursor) {
        FavoriteFeedListCursorAdapter feedListCursorAdapter = new FavoriteFeedListCursorAdapter(this, cursor);
        mRecyclerView.setAdapter(feedListCursorAdapter);
        mRecyclerView.addItemDecoration(new ItemDivider(this));
        feedListCursorAdapter.setOnTemClickListener(new ListItemClickListener(this));
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public class FavoriteFeedsLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
            return new FavoriteFeedLoader(FavoriteFeedActivity.this, bundle.getString(BUNDLE_TABLE),
                    bundle.getStringArray(BUNDLE_PROJECTION), bundle.getString(BUNDLE_SELECTION), null, bundle.getString(BUNDLE_ORDER_BY));
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            cursor.setNotificationUri(getContentResolver(), FeedContentProvider.CONTENT_URI_FEEDS);
            setAdapter(cursor);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    }
}
