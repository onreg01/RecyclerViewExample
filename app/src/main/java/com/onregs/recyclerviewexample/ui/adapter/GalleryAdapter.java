package com.onregs.recyclerviewexample.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.ui.adapter.holder.GalleryViewHolder;

/**
 * Created by vadim on 16.12.2015.
 */
public class GalleryAdapter extends CursorRecyclerViewAdapter<GalleryViewHolder> {

    public GalleryAdapter(Context context, Cursor cursor) {
        super(context, cursor);
    }

    @Override
    public GalleryViewHolder newViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.gallery_item, viewGroup, false);
        return new GalleryViewHolder(view);
    }

    @Override
    public void bindViewHolder(GalleryViewHolder viewHolder, Cursor cursor) {
        viewHolder.bindView(cursor, getContext());
    }

    @Override
    public void onViewRecycled(GalleryViewHolder holder) {
        super.onViewRecycled(holder);
    }


}
