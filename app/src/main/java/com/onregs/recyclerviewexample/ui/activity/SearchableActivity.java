package com.onregs.recyclerviewexample.ui.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.db.content_provider.FeedContentProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.SuggestionProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.async_query_handler.CustomAsyncQueryHandler;
import com.onregs.recyclerviewexample.listener.ListItemClickListener;
import com.onregs.recyclerviewexample.ui.adapter.FeedListCursorAdapter;
import com.onregs.recyclerviewexample.ui.adapter.decorator.ItemDivider;

public class SearchableActivity extends AppCompatActivity implements CustomAsyncQueryHandler.AsyncQueryListener {

    public static final String LIKE = " like ?";
    private static final String SELECTION = Feed.FeedEntry.COLUMN_FEED_TITTLE + LIKE + " or " + Feed.FeedEntry.COLUMN_FEED_DESCRIPTION + LIKE;
    private static final String SHORT_TYPE = "DESC";
    public static final String ORDER_BY = Feed.FeedEntry.COLUMN_FEED_CREATED_AT + " " + SHORT_TYPE;
    public static final int SEARCH_FEED_TOKEN = 0;

    private FeedListCursorAdapter mFeedListCursorAdapter;
    private RecyclerView mRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        String query = handleIntent(getIntent());

        setToolbar(query);

        initList();
        new CustomAsyncQueryHandler(getContentResolver()).setQueryListener(this).startQuery(
                SEARCH_FEED_TOKEN, null, FeedContentProvider.CONTENT_URI_FEEDS,
                FeedContentProvider.FEED_PROJECTION, SELECTION,
                new String[]{"%" + query + "%", "%" + query + "%"}, ORDER_BY);
    }

    private void setToolbar(String query) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.results_for, query));
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initList() {
        mRecyclerView = (RecyclerView) findViewById(R.id.search_list);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void initAdapter(Cursor cursor) {
        mFeedListCursorAdapter = new FeedListCursorAdapter(this, cursor);
        mFeedListCursorAdapter.setOnTemClickListener(new ListItemClickListener(this));
        mRecyclerView.setAdapter(mFeedListCursorAdapter);
        mRecyclerView.addItemDecoration(new ItemDivider(this));
    }


    private String handleIntent(Intent intent) {
        String query = "";
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);

            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, SuggestionProvider.AUTHORITY, SuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);
        }
        return query;
    }

    @Override
    public void onQueryComplete(int token, Object cookie, Cursor cursor) {
        initAdapter(cursor);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFeedListCursorAdapter.closeCursor();
    }
}
