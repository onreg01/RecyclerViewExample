package com.onregs.recyclerviewexample.ui.adapter;/*
 * Copyright (C) 2014 skyfish.jy@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;


/**
 * Created by skyfishjy on 10/31/14.
 */

public abstract class CursorRecyclerViewAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    private final DataSetObservable mDataSetObservable = new DataSetObservable();
    private boolean mAutoRequery;
    private Cursor mCursor;
    private boolean mDataValid;
    private Context mContext;
    private int mRowIDColumn;
    private ChangeObserver mChangeObserver;
    private DataSetObserver mDataSetObserver = new MyDataSetObserver();

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;

    private Cursor mHeader;
    private Cursor mFooter;
    private HeaderFooterBinder mHeaderFooterBinder;

    public CursorRecyclerViewAdapter(Context context, Cursor cursor) {
        super();
        init(context, cursor, true);
    }

    public CursorRecyclerViewAdapter(Context context, Cursor cursor, boolean autoRequery) {
        super();
        init(context, cursor, autoRequery);
    }

    protected void init(Context context, Cursor c, boolean autoRequery) {
        boolean cursorPresent = c != null;
        this.mAutoRequery = autoRequery;
        this.mCursor = c;
        this.mDataValid = cursorPresent;
        this.mContext = context;
        this.mRowIDColumn = cursorPresent ? c.getColumnIndexOrThrow("_id") : -1;
        this.mChangeObserver = new ChangeObserver();
        if (cursorPresent) {
            c.registerContentObserver(mChangeObserver);
            c.registerDataSetObserver(mDataSetObserver);
        }
    }

    public Context getContext() {
        return mContext;
    }

    public Cursor getCursor() {
        return mCursor;
    }

    @Override
    public T onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (!mDataValid) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        }
        return newViewHolder(viewGroup, viewType);
    }

    @Override
    public void onBindViewHolder(T viewHolder, int position) {
        if (!mDataValid) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        }

        if (mHeader == null && mFooter == null) {
            if (!mCursor.moveToPosition(position)) {
                throw new IllegalStateException("couldn't move cursor to position " + position);
            }
            bindViewHolder(viewHolder, mCursor);
            return;
        }

        if (mHeader != null && position == 0) {
            mHeaderFooterBinder.bindHeader(viewHolder, mHeader);
            return;
        }

        if (mFooter != null && position == getItemCount() - 1) {
            mHeaderFooterBinder.bindFooter(viewHolder, mFooter);
            return;
        }

        if (!mCursor.moveToPosition(position - 1)) {
            throw new IllegalStateException("couldn't move cursor to position " + position);
        }
        bindViewHolder(viewHolder, mCursor);
    }

    @Override
    public int getItemCount() {
        if (mDataValid && mCursor != null) {
            int itemCount = mCursor.getCount();
            if (mHeader != null) {
                itemCount += 1;
            }
            if (mFooter != null) {
                itemCount += 1;
            }
            return itemCount;
        } else {
            return 0;
        }
    }

    public void changeCursor(Cursor cursor) {
        Cursor old = swapCursor(cursor);
        if (old != null) {
            old.close();
        }
    }

    public void closeCursor() {
        Cursor[] cursors = new Cursor[]{mCursor, mHeader, mFooter};
        for (Cursor cursor : cursors) {
            if (cursor != null) {
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
        }

    }

    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor == mCursor) {
            return null;
        }
        Cursor oldCursor = mCursor;
        if (oldCursor != null) {
            if (mChangeObserver != null) oldCursor.unregisterContentObserver(mChangeObserver);
            if (mDataSetObserver != null) oldCursor.unregisterDataSetObserver(mDataSetObserver);
        }
        mCursor = newCursor;
        if (newCursor != null) {
            if (mChangeObserver != null) newCursor.registerContentObserver(mChangeObserver);
            if (mDataSetObserver != null) newCursor.registerDataSetObserver(mDataSetObserver);
            mRowIDColumn = newCursor.getColumnIndexOrThrow("_id");
            mDataValid = true;
            // notify the observers about the new cursor
            notifyDataSetChanged();
        } else {
            mRowIDColumn = -1;
            mDataValid = false;
            // notify the observers about the lack of a data set
            notifyDataSetInvalidated();
        }
        return oldCursor;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        mDataSetObservable.registerObserver(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        mDataSetObservable.unregisterObserver(observer);
    }

    public void notifyDataSetInvalidated() {
        mDataSetObservable.notifyInvalidated();
    }

    protected void onContentChanged() {
        if (mAutoRequery && mCursor != null && !mCursor.isClosed()) {
            mDataValid = mCursor.requery();
        }
    }

    public abstract T newViewHolder(ViewGroup parent, int viewType);

    public abstract void bindViewHolder(T viewHolder, Cursor cursor);

    private class ChangeObserver extends ContentObserver {
        public ChangeObserver() {
            super(new Handler());
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            onContentChanged();
        }
    }

    private class MyDataSetObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            mDataValid = true;
            notifyDataSetChanged();
        }

        @Override
        public void onInvalidated() {
            mDataValid = false;
            notifyDataSetInvalidated();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mHeader != null && position == 0) {
            return TYPE_HEADER;
        } else if (mFooter != null && position == getItemCount() - 1) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    public void setHeaderFooter(@Nullable Cursor header, @Nullable Cursor footer, HeaderFooterBinder headerFooterBinder) {
        if (header != null) {
            this.mHeader = header;
        }
        if (footer != null) {
            this.mHeader = header;
        }

        this.mHeaderFooterBinder = headerFooterBinder;
    }

    public interface HeaderFooterBinder {
        void bindHeader(RecyclerView.ViewHolder viewHolder, Cursor header);

        void bindFooter(RecyclerView.ViewHolder viewHolder, Cursor footer);
    }
}