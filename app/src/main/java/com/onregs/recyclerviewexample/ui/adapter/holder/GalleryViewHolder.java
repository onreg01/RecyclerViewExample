package com.onregs.recyclerviewexample.ui.adapter.holder;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.MediaContent;
import com.onregs.recyclerviewexample.data.loader.ImageLoader;
import com.onregs.recyclerviewexample.ui.activity.PictureDetailViewActivity;

/**
 * Created by vadim on 16.12.2015.
 */
public class GalleryViewHolder extends RecyclerView.ViewHolder implements ImageLoader.Callbacks {

    private ImageView mGalleryImage;

    private static final String TYPE_MUSIC = "music";
    private static final String TYPE_PICTURE = "picture";
    private static final String TYPE_VIDEO = "video";

    public GalleryViewHolder(View itemView) {
        super(itemView);
        mGalleryImage = (ImageView) itemView.findViewById(R.id.gallery_item);
    }

    public void bindView(Cursor cursor, Context context) {

        String link = cursor.getString(cursor.getColumnIndex(MediaContent.MediaContentEntry.COLUMN_FIELD_LINK));
        switch (cursor.getString(cursor.getColumnIndex(MediaContent.MediaContentEntry.COLUMN_MEDIA_TYPE))) {

            case TYPE_PICTURE:
                ImageLoader.getInstance(context).load(link, true, this);
                setImageListener(link, context);
                break;
            case TYPE_MUSIC:
                mGalleryImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_music_note));
                setMediaListener(link, context);
                break;
            case TYPE_VIDEO:
                mGalleryImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_filmstrip));
                setMediaListener(link, context);
                break;
        }
    }

    private void setImageListener(final String link, final Context context) {
        mGalleryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PictureDetailViewActivity.class);
                intent.putExtra(MediaContent.MediaContentEntry.COLUMN_FIELD_LINK, link);
                context.startActivity(intent);
            }
        });
    }

    private void setMediaListener(final String link, final Context context) {
        mGalleryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public void successfully(Bitmap bitmap) {
        mGalleryImage.setImageBitmap(bitmap);
    }

    @Override
    public void failLoad(Bitmap bitmap) {
        mGalleryImage.setImageBitmap(bitmap);
    }
}
