package com.onregs.recyclerviewexample.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.ui.adapter.holder.CommentsViewHolder;
import com.onregs.recyclerviewexample.ui.adapter.holder.DetailViewHeaderHolder;

/**
 * Created by vadim on 13.12.2015.
 */
public class DetailViewCursorAdapter extends CursorRecyclerViewAdapter implements CursorRecyclerViewAdapter.HeaderFooterBinder {

    private DetailViewHeaderHolder mDetailViewHeaderHolder;
    private String mCurrentFeedId;

    public DetailViewCursorAdapter(Context context, Cursor cursor, String currentFeedId) {
        super(context, cursor);
        this.mCurrentFeedId = currentFeedId;
    }

    @Override
    public RecyclerView.ViewHolder newViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.detail_view_header, viewGroup, false);
            mDetailViewHeaderHolder = new DetailViewHeaderHolder(view, getContext());
            return mDetailViewHeaderHolder;
        } else {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.comment_item, viewGroup, false);
            return new CommentsViewHolder(view);
        }
    }

    @Override
    public void bindViewHolder(RecyclerView.ViewHolder viewHolder, Cursor cursor) {
        ((CommentsViewHolder) viewHolder).bindView(cursor);
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        if (holder instanceof DetailViewHeaderHolder) {
            mDetailViewHeaderHolder.closeCursor();
        }
    }

    @Override
    public void closeCursor() {
        mDetailViewHeaderHolder.closeCursor();
        super.closeCursor();
    }

    @Override
    public void bindHeader(RecyclerView.ViewHolder viewHolder, Cursor viewHeader) {
        ((DetailViewHeaderHolder) viewHolder).bindView(viewHeader, mCurrentFeedId);
    }

    @Override
    public void bindFooter(RecyclerView.ViewHolder viewHolder, Cursor footer) {

    }
}
