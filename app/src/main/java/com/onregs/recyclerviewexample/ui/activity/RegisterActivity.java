package com.onregs.recyclerviewexample.ui.activity;

import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.AccountPicker;
import com.onregs.recyclerviewexample.Const;
import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.connection.ParseComConnection;
import com.onregs.recyclerviewexample.connection.TokenHandler;
import com.onregs.recyclerviewexample.data.User;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;
import com.onregs.recyclerviewexample.data.loader.UserLoader;
import com.onregs.recyclerviewexample.utils.EditTextChecker;

import java.io.IOException;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, ParseComConnection.AddUserCallbacks {

    public static final int REGISTER_LOADER = 0;
    public static final String EMAIL_BND = "email";
    public static final int REGISTER_TOKEN = 0;
    public static final int SELECTION_ACCOUNT = 2;

    private Button mButtonRegister;
    private EditText mEditTextEmail;
    private EditText mEditTextUserName;
    private EditText mEditTextPassword;
    private EditText mEditTextSecondPassword;

    private TextInputLayout[] mLabels = new TextInputLayout[4];

    private String mEmail;
    private String mUserName;
    private String mPassword;
    private User mUser;

    private CheckEmailCallbacks mCheckEmailCallbacks = new CheckEmailCallbacks();
    private AccessTokenCallbacks mAccessTokenCallbacks = new AccessTokenCallbacks();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setToolBar();
        initEditTexts();
        initLabels();
        initButtonRegister();
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initEditTexts() {
        mEditTextEmail = (EditText) findViewById(R.id.email_edit_text);
        mEditTextUserName = (EditText) findViewById(R.id.user_name_edit_text);
        mEditTextPassword = (EditText) findViewById(R.id.password_edit_text);
        mEditTextSecondPassword = (EditText) findViewById(R.id.second_password_edit_text);
    }

    private void initLabels() {
        mLabels[0] = (TextInputLayout) findViewById(R.id.email_label);
        mLabels[1] = (TextInputLayout) findViewById(R.id.user_name_label);
        mLabels[2] = (TextInputLayout) findViewById(R.id.password_label);
        mLabels[3] = (TextInputLayout) findViewById(R.id.second_password_label);
    }

    private void initButtonRegister() {
        mButtonRegister = (Button) findViewById(R.id.button_register);
        mButtonRegister.setOnClickListener(this);
    }

    private boolean checkDataFields() {
        if (!checkEditTextEmail()) return false;
        if (!checkEditTextUserName()) return false;
        return checkEditTextPasswords();
    }

    private boolean checkEditTextEmail() {
        Editable email = mEditTextEmail.getText();
        if (!EditTextChecker.checkEmailField(this, mLabels[0], email)) {
            return false;
        }
        mEmail = email.toString();
        return true;
    }

    private boolean checkEditTextUserName() {
        Editable userName = mEditTextUserName.getText();
        if (!EditTextChecker.checkIfEmpty(this, mLabels[1], userName)) {
            return false;
        }
        mUserName = userName.toString();
        return true;
    }

    private boolean checkEditTextPasswords() {
        Editable password = mEditTextPassword.getText();
        Editable secondPassword = mEditTextSecondPassword.getText();
        if (!EditTextChecker.checkPasswords(this, mLabels[2], mLabels[3], password, secondPassword)) {
            return false;
        }
        mPassword = password.toString();
        return true;
    }

    @Override
    public void onClick(View v) {
        for (TextInputLayout label : mLabels) {
            label.setErrorEnabled(false);
        }
        if (checkDataFields()) {
            mButtonRegister.setEnabled(false);
            Bundle bundle = new Bundle();
            bundle.putString(EMAIL_BND, mEmail);

            getLoaderManager().restartLoader(REGISTER_LOADER, bundle, mCheckEmailCallbacks);
        }
    }

    private void createUser() {
        mUser = new User();
        mUser.setEmail(mEmail);
        mUser.setUserName(mUserName);
        mUser.setPassword(mPassword);
        mUser.setAuthorized(true);
        setAccessToken();
    }

    private void setAccessToken() {
        Intent intent = AccountPicker.zza(null, null, new String[]{TokenHandler.ACCOUNT_TYPE}, false, null, null, null, null, false, 1, 0);
        startActivityForResult(intent, SELECTION_ACCOUNT);
    }

    public void startFeedActivity() {
        Intent intent = new Intent(RegisterActivity.this, FeedActivity.class);
        intent.putExtra(User.UserEntry.COLUMN_USER_NAME, mUser.getUserName());
        intent.putExtra(User.UserEntry.COLUMN_EMAIL, mUser.getEmail());
        intent.putExtra(User.UserEntry.COLUMN_ACCOUNT_NAME, mUser.getAccountName());
        intent.putExtra(User.UserEntry._ID, mUser.getId());

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    public void successfully(int token, final String objId) {
        mUser.setId(objId);
        new Thread(new Runnable() {
            @Override
            public void run() {
                DataSqliteOpenHelper.getInstance(RegisterActivity.this).addActiveUser(mUser);
            }
        }).start();
        startFeedActivity();
    }

    @Override
    public void error(int handleMessage) {
        if (handleMessage == ParseComConnection.ERROR_ADD_USER) {
            mButtonRegister.setEnabled(true);
            Toast.makeText(this, getString(R.string.error_adding_user), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == SELECTION_ACCOUNT && resultCode == RESULT_OK) {
            String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            mUser.setAccountName(accountName);
            try {
                new TokenHandler(RegisterActivity.this, accountName, mAccessTokenCallbacks).getAccessToken();
            } catch (OperationCanceledException e) {
                Log.e(Const.ERROR, e.getMessage());
                Toast.makeText(this, getString(R.string.account_error), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class AccessTokenCallbacks implements TokenHandler.TokenHandlerCallbacks {

        @Override
        public void run(AccountManagerFuture<Bundle> future) {
            try {
                Bundle bundle = future.getResult();
                mUser.setAccessToken(bundle.getString(AccountManager.KEY_AUTHTOKEN));
                ParseComConnection.addUser(REGISTER_TOKEN, mUser, RegisterActivity.this);
            } catch (android.accounts.OperationCanceledException | IOException | AuthenticatorException e) {
                Log.e(Const.ERROR, e.getMessage());
            }
        }

        @Override
        public boolean handleMessage(Message msg) {
            Toast.makeText(RegisterActivity.this, getString(R.string.get_access_token_error), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private class CheckEmailCallbacks implements LoaderManager.LoaderCallbacks<UserLoader.UserLoaderAnswer> {
        @Override
        public Loader<UserLoader.UserLoaderAnswer> onCreateLoader(int id, Bundle bundle) {
            return new UserLoader(RegisterActivity.this, bundle.getString(EMAIL_BND), null, UserLoader.ACTION_REGISTER);
        }

        @Override
        public void onLoadFinished(Loader loader, UserLoader.UserLoaderAnswer answer) {
            if (answer.getResult()) {
                createUser();
            } else {
                mButtonRegister.setEnabled(true);
                Toast.makeText(RegisterActivity.this, answer.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onLoaderReset(Loader loader) {

        }
    }
}

