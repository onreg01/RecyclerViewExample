package com.onregs.recyclerviewexample.ui.adapter.holder;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.data.FavoriteFeed;
import com.onregs.recyclerviewexample.data.Feed;
import com.onregs.recyclerviewexample.data.db.content_provider.FeedContentProvider;
import com.onregs.recyclerviewexample.data.db.content_provider.async_query_handler.CustomAsyncQueryHandler;
import com.onregs.recyclerviewexample.data.loader.ImageLoader;
import com.onregs.recyclerviewexample.listener.RatingChanger;
import com.onregs.recyclerviewexample.utils.ImageSwitcher;

/**
 * Created by vadim on 27.11.2015.
 */
public class FeedViewHolder extends RecyclerView.ViewHolder implements ImageLoader.Callbacks,
        View.OnClickListener, CustomAsyncQueryHandler.AsyncListener, RatingChanger.RatingChangerCallbacks {

    private TextView mTitle;
    private TextView mDescription;
    private TextView mCommentsCount;
    private ImageView mImageView;
    private TextView mRating;
    private ImageView mFavoriteFeedImage;

    private Context mContext;
    private ImageSwitcher mImageSwitcher;

    private String mCurrentFeedId;
    private boolean mIsFavoriteActivity;

    private AsyncTask mImageLoader;

    public FeedViewHolder(View itemView, Context context, boolean isFavoriteActivity) {
        super(itemView);
        mTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        mDescription = (TextView) itemView.findViewById(R.id.tvDescription);
        mCommentsCount = (TextView) itemView.findViewById(R.id.tvCommentsCount);
        mImageView = (ImageView) itemView.findViewById(R.id.image);
        mRating = (TextView) itemView.findViewById(R.id.rating);
        mFavoriteFeedImage = (ImageView) itemView.findViewById(R.id.feed_favorite);
        mFavoriteFeedImage.setOnClickListener(this);
        mContext = context;
        mIsFavoriteActivity = isFavoriteActivity;
    }

    public void bindView(Cursor cursor) {
        mCurrentFeedId = cursor.getString(cursor.getColumnIndex(FavoriteFeed.FavoriteFeedEntry._ID));

        mTitle.setText(cursor.getString(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_FEED_TITTLE)));
        mDescription.setText(cursor.getString(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_FEED_DESCRIPTION)));
        mCommentsCount.setText(mContext.getString(
                R.string.comments, cursor.getInt(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_FEED_COMMENTS_COUNT))));
        mRating.setText(String.valueOf(cursor.getInt(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_FEED_RATING))));
        mRating.setOnClickListener(
                new RatingChanger((Activity) mContext, mCurrentFeedId, Integer.valueOf(mRating.getText().toString()), this));

        if (mIsFavoriteActivity) {
            setFavoriteIcon(true);
        } else {
            String isFavorite = cursor.getString(cursor.getColumnIndexOrThrow(FavoriteFeed.IS_FAVORITE));
            if (FavoriteFeed.NOT_FAVORITE.equals(isFavorite) || isFavorite == null) {
                setFavoriteIcon(false);
            } else {
                setFavoriteIcon(true);
            }
        }

        int imageSize = (int) mContext.getResources().getDimension(R.dimen.image_size);
        mImageLoader = ImageLoader.getInstance(mContext)
                .load(cursor.getString(cursor.getColumnIndex(Feed.FeedEntry.COLUMN_IMAGE_URL)), imageSize, imageSize, true, this);
    }

    private void setFavoriteIcon(boolean isFavorite) {
        mImageSwitcher = new ImageSwitcher(mFavoriteFeedImage, R.drawable.ic_toggle_star_outline, R.drawable.ic_toggle_star, isFavorite);
    }

    public void clearImage() {
        mImageView.setImageBitmap(null);
        if (mImageLoader != null) {
            mImageLoader.cancel(true);
        }
    }

    @Override
    public void successfully(Bitmap bitmap) {
        mImageView.setImageBitmap(bitmap);
    }

    @Override
    public void failLoad(Bitmap bitmap) {
        mImageView.setImageBitmap(bitmap);
    }

    @Override
    public void onClick(View v) {
        if (mImageSwitcher.onAction()) {
            new CustomAsyncQueryHandler(mContext.getContentResolver()).setListener(this).addFeedToFavorite(mCurrentFeedId);

        } else {
            new CustomAsyncQueryHandler(mContext.getContentResolver()).setListener(this).deleteFeedFromFavorite(mCurrentFeedId);
        }
    }

    protected String getFeedId() {
        return mCurrentFeedId;
    }

    @Override
    public void onComplete() {
        mContext.getContentResolver().notifyChange(FeedContentProvider.CONTENT_URI_FEEDS, null);
    }

    @Override
    public void startPerform() {
        mRating.setEnabled(false);
    }

    @Override
    public void endPerform() {
        mRating.setEnabled(true);
    }
}
