package com.onregs.recyclerviewexample.ui.activity;

import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.onregs.recyclerviewexample.Const;
import com.onregs.recyclerviewexample.R;
import com.onregs.recyclerviewexample.connection.ParseComConnection;
import com.onregs.recyclerviewexample.connection.TokenHandler;
import com.onregs.recyclerviewexample.data.User;
import com.onregs.recyclerviewexample.data.db.DataSqliteOpenHelper;
import com.onregs.recyclerviewexample.data.db.content_provider.FeedContentProvider;
import com.onregs.recyclerviewexample.data.loader.FeedLoader;
import com.onregs.recyclerviewexample.data.loader.LoaderAnswer;
import com.onregs.recyclerviewexample.listener.CustomTabListener;
import com.onregs.recyclerviewexample.settings.Settings;
import com.onregs.recyclerviewexample.ui.adapter.FeedPageAdapter;
import com.onregs.recyclerviewexample.ui.view.SearchViewFormatter;
import com.onregs.recyclerviewexample.utils.DateUtils;
import com.onregs.recyclerviewexample.utils.InternetUtils;

import java.io.IOException;
import java.lang.ref.WeakReference;

public class FeedActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final int CHECK_VALID_TOKEN = 1;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ViewPager mViewPager;
    private FeedPageAdapter mAdapter;
    private TabLayout mTabLayout;
    private DrawerLayout mDrawerLayout;
    private SearchView mSearchView;

    private String mAccountName;
    private String mUserId;

    private FeedDataLoadCallbacks mFeedDataLoadCallbacks = new FeedDataLoadCallbacks();
    private AccessTokenCallbacks mAccessTokenCallbacks = new AccessTokenCallbacks();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        mAccountName = intent.getStringExtra(User.UserEntry.COLUMN_ACCOUNT_NAME);
        mUserId = intent.getStringExtra(User.UserEntry._ID);

        setupNavigationDrawerContent(intent);
        setTabs();
        setViewPager();
        tryLoadDefaultData();
    }

    private void setupNavigationDrawerContent(Intent intent) {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.favorite:
                        startActivityForResult(new Intent(FeedActivity.this, FavoriteFeedActivity.class), Const.FAVORITE_ACTIVITY);
                        break;
                    case R.id.settings:
                        startActivity(new Intent(FeedActivity.this, SettingsActivity.class));
                        break;
                    case R.id.move_checker:
                        startActivity(new Intent(FeedActivity.this, MoveCheckActivity.class));
                        break;
                    case R.id.log_out:
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                        setStatusUser();
                        startActivity(new Intent(FeedActivity.this, LoginActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        break;
                }
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        View view = navigationView.getHeaderView(0);

        ((TextView) view.findViewById(R.id.user_name)).setText(intent.getStringExtra(User.UserEntry.COLUMN_USER_NAME));
        ((TextView) view.findViewById(R.id.user_email)).setText(intent.getStringExtra(User.UserEntry.COLUMN_EMAIL));

    }

    private void setStatusUser() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                DataSqliteOpenHelper.getInstance(FeedActivity.this).deleteActiveUser();
            }
        }).start();
    }

    private void setViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new FeedPageAdapter(getFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mViewPager.setCurrentItem(mTabLayout.getSelectedTabPosition());
        setTouchListener();
    }

    private void setTouchListener() {
        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        mSwipeRefreshLayout.setEnabled(false);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        mSwipeRefreshLayout.setEnabled(true);
                        break;
                }
                return false;
            }
        });
    }

    private void setTabs() {
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);

        String[] tabTittles = getResources().getStringArray(R.array.tabs);
        int selectedPosition = Settings.getInstance(this).getLastTabPosition();

        for (int i = 0; i < 3; i++) {
            mTabLayout.addTab(mTabLayout.newTab().setText(tabTittles[i]), isTabSelected(i, selectedPosition));
        }

        mTabLayout.setOnTabSelectedListener(new CustomTabListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }
        });

        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private boolean isTabSelected(int i, int selectedPosition) {
        return i == selectedPosition;
    }

    private void tryLoadDefaultData() {
        if (DateUtils.checkTimeStamp(this)) {
            setRefreshLayout();
            onRefresh();
        }
    }

    @Override
    public void onRefresh() {
        if (InternetUtils.checkNetwork(this)) {
            try {
                new TokenHandler(this, mAccountName, mAccessTokenCallbacks).getAccessToken();
            } catch (OperationCanceledException e) {
                Log.e(Const.ERROR, e.getMessage());
                Toast.makeText(this, getString(R.string.account_error), Toast.LENGTH_SHORT).show();
                cancelingRefreshLayout();
            }
        } else {
            cancelingRefreshLayout();
            Toast.makeText(this, getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void setRefreshLayout() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    private void cancelingRefreshLayout() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void startFeedLoader() {
        getLoaderManager().restartLoader(0, null, mFeedDataLoadCallbacks);
    }

    ParseComConnection.ConnectionCallbacks connectionCallbacks = new ParseComConnection.ConnectionCallbacks() {

        @Override
        public void successfully(int callbacksToken) {
            if (callbacksToken == CHECK_VALID_TOKEN) {
                startFeedLoader();
            }
        }

        @Override
        public void error(int errorToken) {
            if (errorToken == ParseComConnection.ERROR_VALID_TOKEN) {
                tokenErrorHandle();
            }
        }
    };

    public class AccessTokenCallbacks implements TokenHandler.TokenHandlerCallbacks {

        @Override
        public void run(AccountManagerFuture<Bundle> future) {
            try {
                Bundle bundle = future.getResult();
                String accessToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                ParseComConnection.checkValidAccessToken(CHECK_VALID_TOKEN, accessToken, mUserId, connectionCallbacks);
            } catch (android.accounts.OperationCanceledException | IOException | AuthenticatorException e) {
                Log.e(Const.ERROR, e.getMessage());
                tokenErrorHandle();
            }
        }

        @Override
        public boolean handleMessage(Message msg) {
            tokenErrorHandle();
            return false;
        }
    }

    private void tokenErrorHandle() {
        cancelingRefreshLayout();
        Toast.makeText(FeedActivity.this, getString(R.string.get_access_token_error), Toast.LENGTH_SHORT).show();
    }

    private class FeedDataLoadCallbacks implements LoaderManager.LoaderCallbacks<LoaderAnswer> {

        @Override
        public Loader<LoaderAnswer> onCreateLoader(int id, Bundle args) {
            return new FeedLoader(FeedActivity.this);
        }

        @Override
        public void onLoadFinished(Loader<LoaderAnswer> loader, LoaderAnswer loaderAnswer) {
            cancelingRefreshLayout();
            mSwipeRefreshLayout.setEnabled(true);
            if (mAdapter != null && loaderAnswer.getResult()) {
                getContentResolver().notifyChange(FeedContentProvider.CONTENT_URI_FEEDS, null);
            }
            getLoaderManager().destroyLoader(0);
            Toast.makeText(FeedActivity.this, loaderAnswer.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onLoaderReset(Loader<LoaderAnswer> loader) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.feed_activity_menu, menu);
        MenuItem searchMenu = menu.findItem(R.id.search_menu);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchMenu);

        SearchViewListener searchViewListener = new SearchViewListener();
        mSearchView.setOnSuggestionListener(searchViewListener);
        mSearchView.setOnQueryTextListener(searchViewListener);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        ComponentName componentName = new ComponentName(this, SearchableActivity.class);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(componentName));

        new SearchViewFormatter().setSearchTextColorResource(getResources().getColor(R.color.white))
                .setSearchHintColorResource(getResources().getColor(R.color.white_hint))
                .setCursorColorResource(R.drawable.search_view_cursor).format(mSearchView);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean tryCloseSearchView() {
        if (mSearchView != null) {
            if (!mSearchView.isIconified()) {
                mSearchView.onActionViewCollapsed();
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (!tryCloseSearchView()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Settings.getInstance(this).setLastTabPosition(mTabLayout.getSelectedTabPosition());
    }

    private class SearchViewListener implements SearchView.OnQueryTextListener, SearchView.OnSuggestionListener {

        @Override
        public boolean onQueryTextSubmit(String query) {
            tryCloseSearchView();
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }

        @Override
        public boolean onSuggestionSelect(int position) {
            return false;
        }

        @Override
        public boolean onSuggestionClick(int position) {
            tryCloseSearchView();
            return false;
        }
    }
}
